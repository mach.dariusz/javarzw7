package com.sda.javarzw7.wprowadzenie.week2.oop.interfaces.task;

public abstract class Animal implements IAnimal {
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public abstract void feed();

    @Override
    public String toString() {
        return "Animal name: " + this.name;
    }
}
