package com.sda.javarzw7.wprowadzenie.week2.oop.polymorphism.animals;

public class Cat extends Animal {

    public Cat(String name) {
        super(name);
    }

    @Override
    public void sound() {
        System.out.printf("My name is %s, miau miau miau", this.getName());
    }
}
