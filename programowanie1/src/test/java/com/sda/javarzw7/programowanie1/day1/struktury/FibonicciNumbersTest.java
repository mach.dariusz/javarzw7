package com.sda.javarzw7.programowanie1.day1.struktury;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FibonicciNumbersTest {

    @Test
    void calcFibonacciNumberRecursion() {

        Assertions.assertEquals(FibonicciNumbers.calcFibonacciNumberRecursion(9), 34);
    }
}