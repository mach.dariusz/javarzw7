package com.sda.javarzw7.wprowadzenie.week2.encapsulation;

public class Human {

    private String firstName;
    private String lastName;
    private Gender gender;

    public Human(String firstName, String lastName, Gender gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
    }
}

enum Gender {
    MALE,
    FEMALE
}