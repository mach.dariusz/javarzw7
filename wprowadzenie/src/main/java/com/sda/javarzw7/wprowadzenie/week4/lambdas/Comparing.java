package com.sda.javarzw7.wprowadzenie.week4.lambdas;

import java.util.Arrays;
import java.util.Comparator;

public class Comparing {

    static String[] arrayOfStrings;

    public static void main(String[] args) {
        arrayOfStrings = new String[]{
                "Darek", "Ania", "Oskar",
                "Piotr", "Gosia", "Kasia",
                "Monika", "Łukasz", "Ola"
        };
        System.out.println("Przed sortowaniem: " + Arrays.toString(arrayOfStrings));

        oldVersion();
        System.out.println("--------------------------------------------------------------");
        newVersion();
    }

    private static void oldVersion() {
        Arrays.sort(arrayOfStrings, new MyCustomLengthASCComparator());
        System.out.println("Po sortowaniu asc: " + Arrays.toString(arrayOfStrings));
        Arrays.sort(arrayOfStrings, new MyCustomLengthDESCComparator());
        System.out.println("Po sortowaniu desc: " + Arrays.toString(arrayOfStrings));

        Arrays.sort(arrayOfStrings, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.charAt(0) - s2.charAt(0);
            }
        });

        System.out.println("Po sortowaniu customowym 1: " + Arrays.toString(arrayOfStrings));

        Arrays.sort(arrayOfStrings, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.charAt(s1.length() - 1) - s2.charAt(s2.length() - 1);
            }
        });

        System.out.println("Po sortowaniu customowym 2: " + Arrays.toString(arrayOfStrings));

        Arrays.sort(arrayOfStrings, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareTo(s2);
            }
        });

        System.out.println("Po sortowaniu customowym 3: " + Arrays.toString(arrayOfStrings));

        Arrays.sort(arrayOfStrings, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s2.compareTo(s1);
            }
        });

        System.out.println("Po sortowaniu customowym 4: " + Arrays.toString(arrayOfStrings));

    }


    // with lambdas
    private static void newVersion() {

        Arrays.sort(arrayOfStrings, (s1, s2) -> s1.charAt(0) - s2.charAt(0));

        System.out.println("Po sortowaniu customowym 1: " + Arrays.toString(arrayOfStrings));

        Arrays.sort(arrayOfStrings, (s1, s2) -> s1.charAt(s1.length() - 1) - s2.charAt(s2.length() - 1));

        System.out.println("Po sortowaniu customowym 2: " + Arrays.toString(arrayOfStrings));

        Arrays.sort(arrayOfStrings, (s1, s2) -> s1.compareTo(s2));

        System.out.println("Po sortowaniu customowym 3: " + Arrays.toString(arrayOfStrings));

        Arrays.sort(arrayOfStrings, (String s1, String s2) -> s2.compareTo(s1));

        System.out.println("Po sortowaniu customowym 4: " + Arrays.toString(arrayOfStrings));

    }
}


class MyCustomLengthASCComparator implements Comparator<String> {

    @Override
    public int compare(String o1, String o2) {
        return o1.length() - o2.length(); // po dlugosci asc
    }
}

class MyCustomLengthDESCComparator implements Comparator<String> {

    @Override
    public int compare(String o1, String o2) {
        return o2.length() - o1.length(); // po dlugosci desc
    }
}
