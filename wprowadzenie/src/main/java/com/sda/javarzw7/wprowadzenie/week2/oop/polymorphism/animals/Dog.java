package com.sda.javarzw7.wprowadzenie.week2.oop.polymorphism.animals;

public class Dog extends Animal {

    public Dog(String name) {
        super(name);
    }

    @Override
    public void sound() {
        System.out.printf("My name is %s, hau hau hau", this.getName());
    }
}
