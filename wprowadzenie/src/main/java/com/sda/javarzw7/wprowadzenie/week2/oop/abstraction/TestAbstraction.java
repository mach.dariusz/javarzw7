package com.sda.javarzw7.wprowadzenie.week2.oop.abstraction;

public class TestAbstraction {

    public static void main(String[] args) {
        Vehicle car = new Car("VW");
        System.out.println(car);
        System.out.println("car efficiency: " + car.getEfficiency());

        Vehicle plane = new Plane("Airbus");
        System.out.println(plane);
        System.out.println("plane efficiency: " + plane.getEfficiency());

        // klasa anonimowa
        Vehicle anonymousVehicle = new Vehicle("My custom car") {
            @Override
            public double getDistance() {
                return 100;
            }

            @Override
            public double getFuelUsage() {
                return 2;
            }
        };

        System.out.println(anonymousVehicle);
    }
}
