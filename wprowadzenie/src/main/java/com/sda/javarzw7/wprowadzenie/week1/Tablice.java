package com.sda.javarzw7.wprowadzenie.week1;

public class Tablice {

    public static void main(String[] args) {
        zadanie1();
        zadanie2();
        zadanie3();
        zadanie4(10, 10, '*');
    }

    // Wypisz wszystkie liczby z zakresu 5 -100
    private static void zadanie1() {
        int min = 5;
        int max = 100;

        for (int i = min; i <= max; i++) {
            System.out.print(i);
            if (i != max) {
                System.out.print(", ");
            }
        }

        System.out.println();
    }

    // Wypisz wszystkie liczby z zakresu 1 -100 zaczynając od 100
    private static void zadanie2() {

        int min = 1;
        int max = 100;

        for (int i = max; i >= min; i--) {
            System.out.print(i);
            if (i > min) {
                System.out.print(", ");
            }
        }

        System.out.println();
    }


    // Wypisz wszystkie liczby parzyste z przedziału 0 -100
    private static void zadanie3() {

        int min = 0;
        int max = 100;

        for (int i = min; i <= max; i++) {

            if (i % 2 == 0) {

                System.out.printf("%s %d, kwadrat liczby: %d\n", "Liczba parzysta:", i, i * i);

            }

        }
    }

    // narysuj kwadrat n x n - gdzie:
    // n - moze byc z zakresu 5 - 50
    // znak moze byc rozny, np. *, #, |, $, %
    // a srodek ma byc pusty
    // * * * * *
    // *       *
    // *       *
    // *       *
    // * * * * *
    private static void zadanie4(int x, int y, char znak) {

        for (int i = 0; i < x; i++) {

            for (int j = 0; j < y; j++) {
                if (i == 0 || i == x - 1) {
                    System.out.print(znak + "  ");
                } else if (j == 0 || j == y - 1) {
                    System.out.print(znak + "  ");
                } else {
                    System.out.print("   ");
                }

            }
            System.out.println();
        }

    }
}