package com.sda.javarzw7.programowanie1.day3;

import java.util.Arrays;

public class Bet {

    /**
     * <1,1_000_000>
     *
     * @param searchNumber
     * @param begin        - beginning of a set of numbers
     * @param end          - end of a set of numbers
     * @return the number of steps needed to find the searched number
     */
    public static int calcSteps(int searchNumber, int begin, int end) {
        int steps = 0;
        System.out.println("searchNumber: " + searchNumber);

        // wykonuj, dopoki do sprawdzenia pozostaja wicej niz 2 liczby
        while (end - begin >= 2) {
            steps++;

            int mid = (begin + end) / 2;
            System.out.println("step: " + steps + ", mid: " + mid + ", begin: " + begin + ", end: " + end);

            if (searchNumber == mid) {
                return steps;
            } else if (searchNumber > mid) {
                begin = mid + 1;
            } else {
                end = mid - 1;
            }
        }

        // zostaly tylko 2 liczby do sprawdzenia
        return steps + 1;
    }

    public static int calcStepsRecur(int searchNumber, int begin, int end, int step) {
        int mid = (begin + end) / 2;

        if (searchNumber == mid) {
            return step;
        } else if (searchNumber > mid) {
            begin = mid + 1;
        } else {
            end = mid - 1;
        }

        step++;
        return calcStepsRecur(searchNumber, begin, end, step);
    }


    public static int calcLogN(int number) {
        double result = Math.ceil(Math.log10(number) / Math.log10(2));
        return (int) result;
    }

}
