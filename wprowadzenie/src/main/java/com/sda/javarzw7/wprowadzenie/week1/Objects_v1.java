package com.sda.javarzw7.wprowadzenie.week1;

public class Objects_v1 {

    public static void main(String[] args) {
        przyklad_1();   
        przyklad_2();
        przyklad_3();
    }

    private static void przyklad_3() {
        System.out.println(1 + 2 + "3" + 4 + 5);
        System.out.println(1 + 2 + "3" + (4 + 5));
        System.out.println(1 + 2 + "3" + 4 * 5);
    }

    private static void przyklad_2() {
        String a = "A";
        String b = a;
        System.out.println(a == b);
    }

    private static void przyklad_1() {
        String a = new String("A");
        String b = "A";

        boolean c = a == b;

        System.out.println("a == b -> " + c);
    }
}