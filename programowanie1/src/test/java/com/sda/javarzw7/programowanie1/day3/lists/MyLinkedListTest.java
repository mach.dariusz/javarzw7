package com.sda.javarzw7.programowanie1.day3.lists;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyLinkedListTest {

    @Test
    void insert() {
        MyLinkedList<String> list = new MyLinkedList<>();

        list.insert("Jestem stringiem, a co!");
        list.insert("Ja tez!");
        list.insert("A ja nie chce byc!");

        assertEquals(list.getSize(), 3);
    }

    @Test
    void remove() {
        MyLinkedList<String> list = new MyLinkedList<>();

        list.insert("Jestem stringiem, a co!");
        list.insert("Ja tez!");
        list.insert("A ja nie chce byc!");

        assertTrue(list.remove("Ja tez!"));
        assertEquals(list.getSize(), 2);
    }

}