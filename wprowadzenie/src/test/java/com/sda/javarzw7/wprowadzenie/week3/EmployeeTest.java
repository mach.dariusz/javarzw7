package com.sda.javarzw7.wprowadzenie.week3;

import com.sda.javarzw7.wprowadzenie.week3.collections.Employee;
import org.junit.jupiter.api.Test;

import java.util.*;

public class EmployeeTest {

    @Test
    void test_1() {
        Employee e1 = new Employee(1, "A", "B");
        Employee e2 = new Employee(1, "A", "B");

        // System.out.println(e1 == e2);
        // System.out.println(e1.equals(e2));
    }

    @Test
    void test_2_lists() {
        List<Employee> arrayList = new ArrayList<>();
        List<Employee> linkedList = new LinkedList<>();

        arrayList.add(new Employee(1, "Dariusz", "Mach"));
        arrayList.add(new Employee(2, "Jan", "Kowalski"));

        linkedList.add(new Employee(1, "Dariusz", "Mach"));
        linkedList.add(new Employee(2, "Jan", "Kowalski"));

        boolean arrayListContains = arrayList.contains(new Employee(1, "Dariusz", "Mach"));
        boolean linkedListContains = linkedList.contains(new Employee(1, "Dariusz", "Mach"));

        System.out.println(arrayListContains);
        System.out.println(linkedListContains);

        arrayList.remove(new Employee(1, "Dariusz", "Mach"));

        print(arrayList);
        print(linkedList);
    }


    public static void print(Collection<Employee> collection) {
        for (Employee emp : collection) {
            System.out.println(emp);
        }
    }


    @Test
    void test_sets() {
        Set<Employee> set = new HashSet<>();
        set.add(new Employee(1, "Dariusz", "Mach"));
        set.add(new Employee(1, "Dariusz", "Mach"));
        set.add(new Employee(1, "Dariusz", "Mach"));
        set.add(new Employee(1, "Dariusz", "Mach"));
        set.add(new Employee(1, "Dariusz", "Mach"));
        set.add(new Employee(1, "Dariusz", "Mach"));
        set.add(new Employee(1, "Dariusz", "Mach"));
        set.add(new Employee(1, "Dariusz", "Mach"));
        set.add(new Employee(1, "Dariusz", "Mach"));
        set.add(new Employee(1, "Dariusz", "Mach"));
        set.add(new Employee(1, "Dariusz", "Mach"));
        set.add(new Employee(1, "Dariusz", "Mach"));
        set.add(new Employee(1, "Dariusz", "Mach"));
        set.add(new Employee(1, "Dariusz", "Mach"));
        set.add(new Employee(1, "Dariusz", "Mach"));
        set.add(new Employee(1, "Dariusz", "Mach"));
        set.add(new Employee(1, "Dariusz", "Mach"));

        System.out.println("test_sets()");
        print(set);

        Employee[] array = set.toArray(new Employee[set.size()]);

        System.out.println(Arrays.toString(array));
    }


    @Test
    void test_maps() {
        // K: id, V: Employee(id, firstName, lastName)

        Map<Integer, Employee> mapWithIdAsKey = new HashMap<>();
        mapWithIdAsKey.put(1, new Employee(1, "Dariusz", "Mach"));
        mapWithIdAsKey.put(2, new Employee(2, "Oskar", "Zaremba"));
        mapWithIdAsKey.put(3, new Employee(3, "Monika", "Inglot"));
        mapWithIdAsKey.put(4, new Employee(4, "lukasz", "mozdzen"));

        Employee monika = mapWithIdAsKey.get(3);
        Employee oskar = mapWithIdAsKey.get(2);
        Employee darek = mapWithIdAsKey.get(1);

        System.out.println("test_maps");
        System.out.println(monika);
        System.out.println(oskar);
        System.out.println(darek);

        // pobranie kolekcji pracownikow, wszytskie obiekty (wartosci)
        print(mapWithIdAsKey.values());

        // pobranie set'a kluczy
        Set<Integer> keys = mapWithIdAsKey.keySet();
        for (Integer key : keys) {
            System.out.println(key);
        }

        Set<Map.Entry<Integer, Employee>> entrySet = mapWithIdAsKey.entrySet();

        for (Map.Entry<Integer, Employee> entry : entrySet) {
            System.out.println("klucz: " + entry.getKey() + " | wartosc: " + entry.getValue());
        }


        // new map
        // K: DM, V: Empoyee(id, firstName: "Dariusz", lastName: "Mach")
        System.out.println("**********");
        Map<String, Employee> mapWithStringAsKey = new HashMap<>();

        for (Employee employee : mapWithIdAsKey.values()) {
            String initials = ("" + employee.getFirstName().charAt(0) + employee.getLastName().charAt(0)).toUpperCase();
            mapWithStringAsKey.put(initials, employee);
        }

        Set<Map.Entry<String, Employee>> entries = mapWithStringAsKey.entrySet();

        for (Map.Entry<String, Employee> entry : entries) {
            System.out.println("klucz: " + entry.getKey() + " | wartosc: " + entry.getValue());
        }

    }


    @Test
    void test_maps_counting() {

        String[] strings = {"X","X","X","X","X","X","X","X","X","X","X","X","X","X","X","A", "C", "C", "C", "B", "C", "D", "A", "B", "C", "D", "A", "B", "C", "D", "A", "D", "A", "D", "A", "D", "A", "B", "C", "D", "A", "B", "C", "D", "A", "B", "C", "D", "A", "B", "C", "D", "A", "B", "C", "D"};

        Map<String, Integer> map = new HashMap<>();

        for (String letter: strings) {
            map.put(letter, map.containsKey(letter) ? map.get(letter) + 1 : 1);
        }

        for (String key: map.keySet()) {
            System.out.println("Dla litery " + key + " mamy " + map.get(key) + " wystapien w tablicy");
        }
    }
}
