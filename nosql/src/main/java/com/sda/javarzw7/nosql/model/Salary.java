package com.sda.javarzw7.nosql.model;

import org.springframework.data.annotation.Id;

import java.util.Objects;

public class Salary {

    @Id
    private String id;

    private double salary;

    public Salary() {
    }

    public Salary(double salary) {
        this.salary = salary;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Salary salary1 = (Salary) o;
        return Double.compare(salary1.salary, salary) == 0 &&
                Objects.equals(id, salary1.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, salary);
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Salary{" +
                "id='" + id + '\'' +
                ", salary=" + salary +
                '}';
    }
}
