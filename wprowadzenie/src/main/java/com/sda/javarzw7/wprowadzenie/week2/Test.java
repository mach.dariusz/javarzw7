package com.sda.javarzw7.wprowadzenie.week2;

import com.sda.javarzw7.wprowadzenie.week2.reservation.Customer;
import com.sda.javarzw7.wprowadzenie.week2.reservation.Date;
import com.sda.javarzw7.wprowadzenie.week2.reservation.Trip;

public class Test {

    /**
     * Test class for week 2
     * @param args
     */
    public static void main(String[] args) {
        testPerson1();
        testDog1();
        testDate1();
        testTrip1();
        testCustomer1();
    }

    private static void testPerson1() {
        System.out.println("Test tworzenia klasy Person");
        Person person1 = new Person();
        person1.firstName = "Jan";
        person1.lastName = "Kowalski";
        person1.age = 18;

        Person person2 = new Person("John", "Week");
        person2.age = 60;

        Person person3 = new Person("Dariusz", "Mach", 33);

        System.out.println(person1.lastName + " " + person1.firstName + ", age: " + person1.age);
        System.out.println(person2.lastName + " " + person2.firstName + ", age: " + person2.age);
        System.out.println(person3.lastName + " " + person3.firstName + ", age: " + person3.age);

    }

    private static void testDog1() {
        // TODO
        // napisac klase Dog
        // zmienne: name, breed
        // konstruktor dwu argumentowy
        // stworzyc 3 instancje klasy Dog, nadajac znane Wam wartosc
        Dog dog1 = new Dog("Pluto", "goldi");
        Dog dog2 = new Dog("Bart", "amstaf");
        Dog dog3 = new Dog("Babel", "kundel");
    }

    private static void testDate1() {
        Date start = new Date(1990, 10, 12);
        Date end = new Date(2005, 10, 24);

        System.out.println("start: " + start.year + "-" + start.month + "-" + start.day);
        System.out.println("end: " + end.year + "-" + end.month + "-" + end.day);

        Date today = start;
        System.out.println("today: " + today.year + "-" + today.month + "-" + today.day);

        today.year = 1999;
        today.day = 24;

        System.out.println("today: " + today.year + "-" + today.month + "-" + today.day);
        System.out.println("start: " + start.year + "-" + start.month + "-" + start.day);

        end = today;

        System.out.println("end: " + end.year + "-" + end.month + "-" + end.day);
    }

    private static void testTrip1() {
        Trip trip = new Trip(
                new Date(2019, 8, 5),
                new Date(2019, 8, 15),
                "Miedzyzdroje");
        System.out.println(trip.getInfo());
    }


    private static void testCustomer1() {
        Customer customer = new Customer("Dariusz Mach");
        customer.setAddress("Rzeszów");

        Trip trip = new Trip(
                new Date(2019, 8, 5),
                new Date(2019, 8, 15),
                "Miedzyzdroje");
        customer.assignTrip(trip);

        System.out.println(customer.getInfo());
    }



}
