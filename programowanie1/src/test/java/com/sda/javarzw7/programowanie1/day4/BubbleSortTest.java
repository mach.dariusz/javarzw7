package com.sda.javarzw7.programowanie1.day4;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class BubbleSortTest {

    @Test
    void sort() {

        int[] array = { 7, 4, 2, 5, 1, 3 };
        int[] sortedArray = { 1, 2, 3, 4, 5, 7 };

        BubbleSort.sort(array);

        assertArrayEquals(array, sortedArray);

    }

    @Test
    void sortJava8() {

        int[] array = { 7, 4, 2, 5, 1, 3 };
        int[] sortedArray = { 1, 2, 3, 4, 5, 7 };

        BubbleSort.sortJava8(array);

        assertArrayEquals(array, sortedArray);

    }

    @Test
    void sort_time() {
        int[] array = getRandomArray(100_000, 1000);
        int[] arrayToSort = array.clone();

        System.out.println(Arrays.toString(array));

        LocalDateTime start = LocalDateTime.now();
        Arrays.sort(array);
        LocalDateTime end = LocalDateTime.now();

        System.out.println("quick sort: " + ChronoUnit.MILLIS.between(start, end));

        start = LocalDateTime.now();
        BubbleSort.sort(arrayToSort);
        end = LocalDateTime.now();

        System.out.println("bubble sort: " + ChronoUnit.MILLIS.between(start, end));

        assertArrayEquals(array, arrayToSort);
    }


    private static int[] getRandomArray(int size, int range) {
        int[] array = new int[size];

        Random random = new Random();
        for (int i = 0; i < size; i++) {
            array[i] = random.nextInt(range);
        }

        return array;
    }
}