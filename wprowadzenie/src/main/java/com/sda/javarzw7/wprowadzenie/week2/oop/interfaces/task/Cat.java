package com.sda.javarzw7.wprowadzenie.week2.oop.interfaces.task;

public class Cat extends Animal {

    public Cat(String name) {
        super(name);
    }

    @Override
    public void feed() {
        System.out.println("ale dobra rybka, yami yami");
    }

    @Override
    public void sound() {
        System.out.println("miau miau miau");
    }
}
