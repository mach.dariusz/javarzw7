package com.sda.javarzw7.wprowadzenie.lastday;

import java.io.Serializable;

public interface ITodoList extends Serializable {

    /**
     * This method adds Todo object to list
     *
     * @param title
     * @param description
     * @param priority
     */
    void add(String title, String description, Priority priority);

    /**
     * Removes Todo from list using id
     * @param id
     */
    void remove(int id);

    /**
     * Updates finished field in Todo object
     * @param id
     */
    void markAsFinished(int id);

    /**
     * Show all Todos
     */
    void show();

}
