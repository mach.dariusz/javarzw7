package com.sda.javarzw7.programowanie1.day1;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.Objects;

public class Review {

    public static void main(String[] args) {
        Gson gson = new Gson();
        System.out.println(gson.toJson(new Employee("Dariusz Mach")));

        Employee e1 = new Employee("1");
        Employee e2 = new Employee("1");

        System.out.println(e1 == e2); // false
        System.out.println(e1.equals(e2)); // true
    }
}


class Employee implements Serializable {

    private static final long serialVersionUID = 2L; // do sprawdzenia wersji obiektu

    private String name;
    transient private Address address;

    public Employee(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {

        // Employee passedObject = (Employee) obj;
        return this.name.equals( ((Employee) obj).name );
    }
}

class Address {}