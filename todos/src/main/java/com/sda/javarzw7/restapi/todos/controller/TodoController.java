package com.sda.javarzw7.restapi.todos.controller;

import com.sda.javarzw7.restapi.todos.model.Todo;
import com.sda.javarzw7.restapi.todos.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// @CrossOrigin(origins = "*") // allows for all services
@CrossOrigin(origins = "http://localhost:4200") // allows for http://localhost:4200
@RestController()
@RequestMapping("/api/v1")
public class TodoController {

    @Autowired
    private TodoService todoService;

    @GetMapping("/todos")
    public ResponseEntity<List<Todo>> getAll() {

        return ResponseEntity.ok(todoService.getAll());
    }

    @GetMapping("/todos/{id}")
    public ResponseEntity<Todo> get(@PathVariable("id") long id) {

        return todoService.get(id) == null ? new ResponseEntity(HttpStatus.NOT_FOUND) : ResponseEntity.ok(todoService.get(id));
    }

    @PostMapping("/todos")
    public ResponseEntity<Todo> add(@RequestBody() String content) {

        return new ResponseEntity(todoService.add(content), HttpStatus.CREATED);
    }

    @DeleteMapping("/todos/{id}")
    public ResponseEntity<Todo> remove(@PathVariable("id") long id) {

        return new ResponseEntity(todoService.remove(id), HttpStatus.OK);
    }

    @PatchMapping("/todos/{id}")
    public ResponseEntity<Todo> markAsFinished(@PathVariable("id") long id) {

        return new ResponseEntity(todoService.markAsFinished(id), HttpStatus.OK);
    }

}
