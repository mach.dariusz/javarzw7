package com.sda.javarzw7.wprowadzenie.week2.encapsulation;

public class Employee {

    private String firstName;
    private String lastName;
    private final long pesel;

    public Employee(long pesel) {
        this.pesel = pesel;
    }

    public Employee(String firstName, String lastName, long pesel) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getPesel() {
        return pesel;
    }
}

