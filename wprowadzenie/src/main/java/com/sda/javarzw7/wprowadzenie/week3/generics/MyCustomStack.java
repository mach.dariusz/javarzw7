package com.sda.javarzw7.wprowadzenie.week3.generics;

import java.util.Arrays;

public class MyCustomStack<T> { // LIFO

    // "Czarek" | Integer(1)  | Employee()
    // "Marek"  | Integer(2)  | Employee()
    // "Darek"  | Integer(3)  | Employee()

    private T[] stack;
    private int capacity;
    private int size;

    public MyCustomStack(int capacity) {
        this.capacity = capacity;
        this.stack = (T[]) new Object[capacity];
        this.size = 0;
    }

    public boolean push(T object) {
        if (!isFull()) {
            this.stack[this.size] = object;
            this.size++;

            return true;
        } else {
            return false;
        }
    }

    private boolean isFull() {
        return this.size == this.capacity;
    }

    public String getInfo() {
        return Arrays.toString(this.stack);
    }

}
