package com.sda.javarzw7.programowanie1.day4;

import java.lang.reflect.Array;
import java.util.Arrays;

public class QuickSort {

    public static void sort(int[] array, int begin, int end) {

        if (begin < end) {
            // System.out.println("Sorting array " + Arrays.toString(array) + " from " + begin + " to " + end);
            int partitionIndex = partition(array, begin, end);

            sort(array, begin, partitionIndex - 1); // left sort
            sort(array, partitionIndex + 1, end); // right sort
        }

    }

    /**
     *
     * @param array
     * @param begin
     * @param end
     *
     * @return pivot final position after partition
     */
    private static int partition(int[] array, int begin, int end) {

        int pivot = array[end]; // set pivot
        int i = begin;

        for (int j = begin; j < end; j++) {
            if (array[j] <= pivot) { // swap only those elements which are less than or equals pivot
                // swap
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;

                // increment position index
                i++;
            }
        }

        // swap first element which is grater than pivot
        int swapTemp = array[i];
        array[i] = array[end];
        array[end] = swapTemp;

        return i;
    }
}
