package com.sda.javarzw7.wprowadzenie.week2.oop.interfaces;

public interface MyInterface {

    double calculate(double x);
}
