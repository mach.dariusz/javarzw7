package com.sda.javarzw7.wprowadzenie.week2.oop.polymorphism.employees;

public enum Type {
    OTHER {
        public double getSalary() {
            return 10_000.00;
        }
    }, // $10K

    INTERN {
        public double getSalary() {
            return 5_000.00;
        }
    }, // $5K

    MANGER {
        public double getSalary() {
            return 40_000.00;
        }
    }, // $40K

    WORKER {
        public double getSalary() {
            return 20_000.00;
        }
    }, // $20K

    WHITE_COLLAR_WORKER {
        public double getSalary() {
            return 30_000.00;
        }
    }; // $30K

    public double getSalary() {
        throw new AbstractMethodError();
    }
}
