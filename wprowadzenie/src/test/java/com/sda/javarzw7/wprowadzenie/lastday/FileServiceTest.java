package com.sda.javarzw7.wprowadzenie.lastday;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class FileServiceTest {

    TodoList todoList;

    @BeforeEach
    void beforeEach() {
        todoList = new TodoList();

        todoList.add("Nauka Javy", "Wiecej czasu spedzic na nauce", Priority.HIGH);
        todoList.add("Nauka testowania kodu", "Przegladnac przyklady z zajec i wyprobowac na swoich klasach", Priority.MEDIUM);
        todoList.add("Odpoczynek", "Pojechac na jakies dobre wakacje", Priority.LOW);
    }

    @DisplayName("Save to and read from file")
    @Test
    void test() {

        System.out.println("Lista przed operacjami zapisu i odczytu:");
        todoList.show();

        FileService.saveTodoList("todoListForTesting.txt", todoList);
        FileService.saveTodoList("todoListForTesting.txt", todoList); // to nadpisze dane w pliku

        TodoList todoListFromFile = FileService.readTodoList("todoListForTesting.txt");

        System.out.println();
        System.out.println("Lista po operacjach zapisu i odczytu:");
        todoListFromFile.show();

        Assertions.assertEquals(todoList, todoListFromFile);
        Assertions.assertEquals(todoList.getTodo(1).toString(), todoListFromFile.getTodo(1).toString());

    }
}