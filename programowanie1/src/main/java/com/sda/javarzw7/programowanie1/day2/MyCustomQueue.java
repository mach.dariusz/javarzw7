package com.sda.javarzw7.programowanie1.day2;

public class MyCustomQueue<T> implements IMyCustomQueue<T> {

    private T[] queue;
    private int size;
    private int head;
    private int tail;

    public MyCustomQueue(int capacity) {
        this.queue = (T[]) new Object[capacity];
        this.size = 0;
        this.head = 0;
        this.tail = 0;
    }

    @Override
    public void push(T object) {
        if (isFull()) {
            throw new QueueIsFullException();
        } else {
            this.queue[this.head] = object;
            this.head = (this.head + 1) % getCapactity();
            this.size++;
        }
    }

    @Override
    public T pop() {
        if (isEmpty()) {
            return null;
        } else {
            T tempObject = this.queue[this.tail];
            this.queue[this.tail] = null;
            this.size--;
            this.tail = (this.tail + 1) % getCapactity();
            return tempObject;
        }
    }

    @Override
    public T peek() {
        if (isEmpty()) {
            return null;
        } else {
            return this.queue[this.tail];
        }
    }

    public int getCapactity() {
        return this.queue.length;
    }

    public int getSize() {
        return this.size;
    }

    public boolean isFull() {
        return this.size == getCapactity();
    }

    public boolean isEmpty() {
        return this.size == 0;
    }
}

class QueueIsFullException extends RuntimeException {}
