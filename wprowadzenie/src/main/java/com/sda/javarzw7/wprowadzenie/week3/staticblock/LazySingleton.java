package com.sda.javarzw7.wprowadzenie.week3.staticblock;

public class LazySingleton {
    static {
        System.out.println("Block static {} w LazySingleton");
    }

    private LazySingleton() {
        System.out.println("Konstruktor LazySingleton");
    };

    private static class LazyHolder {
        static {
            System.out.println("Block static {} w private static class LazyHolder");
        }

        static final LazySingleton INSTANCE = new LazySingleton();
    }

    public synchronized static LazySingleton getInstance() {
        System.out.println("getInstance()");
        return LazyHolder.INSTANCE;
    }

}


class Test {
    static {
        System.out.println("Block static {} w class Test");
    }

    public static void main(String[] args) {
        System.out.println("main()");
        LazySingleton singleton1 = LazySingleton.getInstance();
        LazySingleton singleton2 = LazySingleton.getInstance();
        LazySingleton singleton3 = LazySingleton.getInstance();
        LazySingleton singleton4 = LazySingleton.getInstance();
        LazySingleton singleton5 = LazySingleton.getInstance();
        LazySingleton singleton6 = LazySingleton.getInstance();

        System.out.println(singleton1 == singleton2);
        System.out.println(singleton3 == singleton6);


    }
}