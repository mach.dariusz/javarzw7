package com.sda.javarzw7.programowanie1.day5.threads;

import java.util.concurrent.TimeUnit;

public class Deadlock {

    public static Object lock1 = new Object();
    public static Object lock2 = new Object();

    public static void main(String[] args) throws InterruptedException {
        ThreadDemo1 threadDemo1 = new ThreadDemo1();
        ThreadDemo2 threadDemo2 = new ThreadDemo2();

        TimeUnit.SECONDS.sleep(10);

        threadDemo1.start();
        threadDemo2.start();
    }

    private static class ThreadDemo1 extends Thread {
        @Override
        public void run() {
            synchronized (lock1) {
                System.out.println("Thread 1: Holding lock1 ...");

                // try {
                //     Thread.sleep(10);
                // } catch (InterruptedException e) {
                //     // e.printStackTrace();
                // }

                System.out.println("Thread 1: Wating for lock2 ...");

                synchronized (lock2) {
                    System.out.println("Thread 1: Holding lock1 and lock2 ...");
                }
            }
        }
    }

    private static class ThreadDemo2 extends Thread {
        @Override
        public void run() {
            synchronized (lock2) {
                System.out.println("Thread 2: Holding lock2 ...");

                // try {
                //     Thread.sleep(10);
                // } catch (InterruptedException e) {
                //     // e.printStackTrace();
                // }

                System.out.println("Thread 2: Wating for lock1 ...");

                synchronized (lock1) {
                    System.out.println("Thread 2: Holding lock2 and lock1 ...");
                }
            }
        }
    }

}
