package com.sda.javarzw7.wprowadzenie.week3;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Trip {

    private Date start;
    private Date end;
    private String destination;
    protected double price;

    public Trip(Date start, Date end, String destination, double price) {
        this.start = start;
        this.end = end;
        this.destination = destination;
        this.price = price;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    public String getDestination() {
        return destination;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy"); // 01-04-2019

        return String.format("Trip destination: %s, %s - %s, price: %.2f PLN",
                destination,
                formatter.format(start),
                formatter.format(end),
                price);
    }
}
