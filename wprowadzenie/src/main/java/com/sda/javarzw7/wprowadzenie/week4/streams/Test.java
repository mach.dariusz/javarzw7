package com.sda.javarzw7.wprowadzenie.week4.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Test {

    private static List<Employee> employeeList = new ArrayList();

    public static void main(String[] args) {

        init();
        print(employeeList);

        stream_1();
        stream_2();
        stream_3();
        stream_3a();
        stream_3b();

    }

    private static void print(List list) {
        System.out.println("Employee list:");
        for (Object object : list) {
            System.out.println(object);
        }
    }


    private static void init() {
        employeeList.add(new Employee(1, "Oskar", "Zaremba"));
        employeeList.add(new Employee(2, "Łukasz", "Możdżeń"));
        employeeList.add(new Employee(3, "Monika", "Inglot"));
        employeeList.add(new Employee(4, "Jaromir", "Kielar"));
        employeeList.add(new Employee(5, "Katarzyna", "Grata-Bugaj"));
        employeeList.add(new Employee(6, "Kamil", "Piękoś"));
    }

    private static void stream_1() {
        System.out.println("\nstream_1()");

        // one way
        employeeList.stream().forEach(new Consumer<Employee>() {
            @Override
            public void accept(Employee employee) {
                System.out.println(employee);
            }
        });

        System.out.println("********** with streams ************");

        // second way
        employeeList.stream().forEach(employee -> System.out.println(employee));

        // both do the same
    }

    private static void stream_2() {
        System.out.println("\nstream_2()");

        employeeList.stream().filter(new Predicate<Employee>() {
            @Override
            public boolean test(Employee employee) {
                return employee.getId() > 3;
            }
        }).forEach(employee -> System.out.println(employee));

        System.out.println("********** with streams ************");

        employeeList.stream()
                .filter(employee -> employee.getId() > 3) // przefiltruj i przekaz dalej tylko pracownikow z id wiekszym od 3
                .forEach(employee -> System.out.println(employee)); // wydrukuj tych pracownikow, ktorzy przeszli przez filter

    }

    private static void stream_3() {
        System.out.println("\nstream_3()");

        List<String> list_1 = employeeList.stream().map(new Function<Employee, String>() {
            @Override
            public String apply(Employee employee) {
                return employee.getLastName() + " " + employee.getFirstName();
            }
        }).collect(Collectors.toList());

        print(list_1);

        System.out.println("********** with streams ************");

        List<String> list_2 = employeeList.stream()
                .map(employee -> employee.getLastName() + " " + employee.getFirstName())
                .collect(Collectors.toList());

        print(list_2);

    }

    private static void stream_3a() {
        System.out.println("\nstream_3a()");

        List<Manager> managerList = employeeList.stream()
                .map(employee -> new Manager(employee))
                .collect(Collectors.toList());

        // both do the same
        managerList.forEach(System.out::println);
        System.out.println("****** ******** ********");
        managerList.forEach(manager -> System.out.println(manager));
    }

    private static void stream_3b() {
        System.out.println("\nstream_3b()");

        int result = employeeList.stream()
                .map(employee -> employee.getId())
                .reduce((x,y) -> x + y).get();
        System.out.println(result);

        int result2 = employeeList.stream()
                .map(Employee::getId)
                .reduce((x,y) -> {
                    // for debugging
                    System.out.println("x = " + x);
                    System.out.println("y = " + y);
                    return  x + y;
                }).get();
        System.out.println(result2);
    }
}

class Manager extends Employee {

    public Manager(Employee employee) {
        super(employee.getId(), employee.getFirstName(), employee.getLastName());
    }


    @Override
    public String toString() {
        return "Manager: " + super.toString();
    }
}