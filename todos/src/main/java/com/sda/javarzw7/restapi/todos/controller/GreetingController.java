package com.sda.javarzw7.restapi.todos.controller;

import com.sda.javarzw7.restapi.todos.model.Greeting;
import com.sda.javarzw7.restapi.todos.service.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/api/v1")
public class GreetingController {

    @Autowired
    private GreetingService greetingService;

    private String template = "Hello %s!";

    @RequestMapping("/greeting")
    public Greeting showGreeting(@RequestParam(value = "content", defaultValue = "world") String content) {

        return new Greeting(greetingService.checkContent(content), String.format(template, content));
    }
}
