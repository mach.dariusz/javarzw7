package com.sda.javarzw7.programowanie1.day4;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.stream.IntStream;

public class BubbleSort {

    public static void sort(int[] array) {

        // System.out.println("Started array: " + Arrays.toString(array));
        // System.out.println();

        for (int i = 0; i < array.length; i++) {
            boolean wasSwap = false;

            // System.out.println("Turn " + (i + 1));

            for (int j = 1; j < array.length - i; j++) {
                // System.out.printf("array[%d] > array[%d] -> %d > %d %n", j - 1, j, array[j - 1], array[j]);

                if (array[j - 1] > array[j]) {
                    // System.out.printf("\t-------> swap %d with %d %n", array[j - 1], array[j]);

                    int temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;

                    wasSwap = true;
                }


            }

            // System.out.printf("\nAfter %d -> %s %n", i + 1, Arrays.toString(array));
            // System.out.println();

            if (!wasSwap) return;
        }
    }


    public static void sortJava8(int[] array) {
        int n = array.length;

        IntStream.range(0, n - 1)
                .flatMap(i -> {
                    System.out.println("\ni: " + i);
                    return IntStream.range(1, n - i);
                }).forEach(j -> {

            System.out.printf("array[%d] > array[%d] -> %d > %d %n", j - 1, j, array[j - 1], array[j]);
            if (array[j - 1] > array[j]) {
                int temp = array[j - 1];
                array[j - 1] = array[j];
                array[j] = temp;
            }

        });
    }

}
