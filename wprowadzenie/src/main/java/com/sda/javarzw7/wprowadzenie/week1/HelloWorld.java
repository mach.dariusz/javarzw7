package com.sda.javarzw7.wprowadzenie.week1;

import java.util.Arrays;

public class HelloWorld { // HelloWorld.java  <- line comment

    public static void main(String[] args) {

        System.out.println("Hello World!!!");
        System.out.println();
        System.out.println("Ilosc argumentow podanych do aplikacji wynosi: " + args.length);
        
        // trzeci sposob - najlepszy jak dotad
        System.out.println();
        System.out.println("Trzeci sposob - najlepszy jak dotad");
        for (int i = 0; i < args.length; i++) {
            System.out.print(args[i] + " ");
        }
        
        // drugi sposob - troche lepszy
        System.out.println("\n");
        System.out.println("Drugi sposob - troche lepszy");
        System.out.println(Arrays.toString(args));

        // pierwszy sposob - nie idealny 
        System.out.println();
        System.out.println("Pierwszy sposob - nie idealny");   
        System.out.print(args[0] + " ");
        System.out.print(args[1] + " ");
        System.out.print(args[2] + " ");
        System.out.print(args[3] + " ");
        System.out.print(args[4] + "\n");

    }

}