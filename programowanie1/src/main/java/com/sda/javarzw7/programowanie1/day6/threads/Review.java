package com.sda.javarzw7.programowanie1.day6.threads;

import java.time.LocalTime;
import java.util.concurrent.*;
import java.util.logging.Logger;

public class Review {

    private static final String LOG_CLASS = Review.class.getName();
    private static final Logger LOGGER = Logger.getLogger(Review.class.getName());

    static Runnable runnableTask = () -> {
        // run() implementation from Runnable
        LOGGER.entering(LOG_CLASS, "runnableTask() entry");
        LOGGER.info("Message from run() in Runnable");

        try {
            TimeUnit.SECONDS.sleep(3);

            // throw new Exception();
        } catch (Exception e) {
            LOGGER.throwing(LOG_CLASS, "run() in Runnable", e);
        }

        LOGGER.exiting(LOG_CLASS, "runnableTask() return");
    };

    static Callable<Integer> callableTask = () -> {
        LOGGER.entering(LOG_CLASS, "callableTask() entry");
        boolean shouldFinish = true;
        int counter = 0;

        while (shouldFinish) {
            counter++;
            TimeUnit.SECONDS.sleep(2);
            LOGGER.info("after sleep() -> counter = " + counter);
        }

        LOGGER.exiting(LOG_CLASS, "callableTask() return");
        return 1;
    };

    static Callable<Long> factorialCallableTask = () -> {
        LOGGER.entering(LOG_CLASS, "factorialCallableTask() entry");
        int number = 25;
        long factorial = 1;

        LOGGER.info("Time before calc factorial: " + LocalTime.now().toString());
        for (int count = number; count > 1; count--) {
            TimeUnit.MILLISECONDS.sleep(500);
            factorial = factorial * count;
        }
        LOGGER.info("Time after calc factorial: " + LocalTime.now().toString());

        LOGGER.exiting(LOG_CLASS, "factorialCallableTask() return");
        return factorial;
    };

    public static void main(String[] args) {

        LOGGER.entering(LOG_CLASS, "main() method");

        LOGGER.info("Message from logger");
        LOGGER.warning("Message from logger");
        LOGGER.severe("Message from logger");
        LOGGER.fine("Message from logger");
        LOGGER.finest("Message from logger");

        Thread runnableExampleThread = new Thread(runnableTask);

        runnableExampleThread.start();

        // ExecutorService executorService = Executors.newSingleThreadExecutor();
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        executorService.submit(runnableTask);
        Future<Long> future = executorService.submit(factorialCallableTask);
        executorService.submit(callableTask);

        executorService.shutdown();
        try {
            LOGGER.info("Waiting for tasks to terminate ...");
            executorService.awaitTermination(20, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.throwing(LOG_CLASS, "callable call() method", e);
        } finally {
            java.util.List<Runnable> listOfTerminatedTasks = executorService.shutdownNow();
            LOGGER.info("Tasks terminated: " + listOfTerminatedTasks.size());
        }

        try {
            LOGGER.info("Result of calc factorial = " + future.get());
        } catch (InterruptedException e) {
            LOGGER.throwing(LOG_CLASS, "calc_factorial", e);
        } catch (ExecutionException e) {
            LOGGER.throwing(LOG_CLASS, "calc_factorial", e);
        }

        LOGGER.exiting(LOG_CLASS, "main() method");
    }
}
