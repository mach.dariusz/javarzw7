package com.sda.javarzw7.wprowadzenie.week3;

import com.sda.javarzw7.wprowadzenie.week3.exceptions.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MathUtilsTest {


    @Test
    void testFactorial_for_0() {
        Assertions.assertEquals(MathUtils.calcFactorial(0), 1);
    }

    @Test
    void testFactorial_for_1() {
        Assertions.assertEquals(MathUtils.calcFactorial(1), 1);
    }

    @Test
    void testFactorial_for_3() {
        Assertions.assertEquals(MathUtils.calcFactorial(3), 6);
    }

    @Test
    void testFactorial_for_20() {

        long start = System.nanoTime();

        long result = MathUtils.calcFactorial(20);

        long end = System.nanoTime();

        System.out.println("testFactorial_for_20 -> Duration: " + (end - start));

        Assertions.assertEquals(result, 2432902008176640000L);
    }

    @Test
    void testFactorialWithLoop_for_3() {
        Assertions.assertEquals(MathUtils.calcFactorialWithLoop(3), 6);
    }

    @Test
    void testFactorialWithLoop_for_20() {
        long start = System.nanoTime();

        long result = MathUtils.calcFactorial(20);

        long end = System.nanoTime();

        System.out.println("testFactorialWithLoop_for_20 -> Duration: " + (end - start));

        Assertions.assertEquals(result, 2432902008176640000L);
    }

    @Test
    void testFactorialWithLoop_for_25() {
        long start = System.nanoTime();

        long result = MathUtils.calcFactorial(25);

        long end = System.nanoTime();

        System.out.println("testFactorialWithLoop_for_25 -> Duration: " + (end - start));

        Assertions.assertEquals(result, 7034535277573963776L);
    }
}
