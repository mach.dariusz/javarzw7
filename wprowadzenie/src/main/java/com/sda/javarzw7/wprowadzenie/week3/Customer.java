package com.sda.javarzw7.wprowadzenie.week3;

public class Customer {

    private int id;
    private String firstName;
    private String lastName;
    private Address address;

    public Customer(int id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Address getAddress() {
        return address;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append("Customer: id: ").append(id);
        sb.append(", name: ").append(lastName).append(" ").append(firstName);

        if (address != null) {
            sb.append(',').append("\n\t").append(address);
        }

        return sb.toString();
    }

}
