package com.sda.javarzw7.programowanie1.day1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class MathUtilsTest {

    @DisplayName("Test calcAbs()")
    @Test
    void test1() {
        Assertions.assertEquals(MathUtils.calcAbs(-100), 100);
        Assertions.assertEquals(MathUtils.calcAbs(2), 2);
    }

    @DisplayName("Test calcFactorial()")
    @Test
    void test2() {
        Assertions.assertThrows(WrongNumberException.class, () -> MathUtils.calcFactorial(-30));
        Assertions.assertEquals(MathUtils.calcFactorial(0),1);
        Assertions.assertEquals(MathUtils.calcFactorial(10), 3628800);
        // Assertions.assertEquals(MathUtils.calcFactorial(26), 3628800); -- poza zakresem longa

        // System.out.println(Integer.MAX_VALUE);
        // System.out.println(Long.MAX_VALUE);
    }

    @DisplayName("Test isPrimary()")
    @Test
    void test3() {
        Assertions.assertThrows(WrongNumberException.class, () -> MathUtils.isPrimary(-2));
        Assertions.assertTrue(MathUtils.isPrimary(11));
        Assertions.assertFalse(MathUtils.isPrimary(12));
    }

}