package com.sda.javarzw7.programowanie1.day2;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyCustomQueueTest {

    @DisplayName("adding to queue")
    @Test
    void test1() {
        MyCustomQueue queue = new MyCustomQueue(5);
        assertEquals(queue.getSize(), 0);
        assertEquals(queue.getCapactity(), 5);

        queue.push("A");
        queue.push("B");
        queue.push("C");

        assertEquals(queue.getSize(), 3);
    }

    @DisplayName("removing from queue")
    @Test
    void test2() {
        MyCustomQueue queue = new MyCustomQueue(5);

        queue.push("A");
        queue.push("B");
        queue.push("C");

        assertEquals(queue.getSize(), 3);

        assertEquals(queue.pop(), "A");
        assertEquals(queue.pop(), "B");
        assertEquals(queue.pop(), "C");

        assertEquals(queue.getSize(), 0);
    }

    @DisplayName("peeking from queue")
    @Test
    void test3() {
        MyCustomQueue queue = new MyCustomQueue(5);

        queue.push("A");
        queue.push("B");
        queue.push("C");

        assertEquals(queue.getSize(), 3);

        assertEquals(queue.peek(), "A");
        assertEquals(queue.peek(), "A");
        assertEquals(queue.peek(), "A");

        assertEquals(queue.getSize(), 3);
    }

    @DisplayName("adding to queue - more items")
    @Test
    void test4() {
        MyCustomQueue queue = new MyCustomQueue(5);
        queue.push("A");
        queue.push("B");
        queue.push("C");
        queue.push("D");
        queue.push("E");

        assertEquals(queue.getSize(), 5);

        queue.pop();
        queue.pop();
        assertEquals(queue.getSize(), 3);

        queue.push("F");
        queue.push("G");
        assertEquals(queue.getSize(), 5);

        queue.pop();
        queue.pop();
        queue.pop();
        queue.pop();
        queue.pop();
        assertEquals(queue.getSize(), 0);

    }

}