package com.sda.javarzw7.wprowadzenie.week2.varargs;

import com.sda.javarzw7.wprowadzenie.week2.reservation.Date;

public class App {

    public static void main(String[] args) {
        System.out.println("Liczba argumentow: " + getArgsLength());
        System.out.println("Liczba argumentow: " + getArgsLength(""));
        System.out.println("Liczba argumentow: " + getArgsLength("",""));
        System.out.println("Liczba argumentow: " + getArgsLength("","",""));
        System.out.println("Liczba argumentow: " + getArgsLength("","","",""));
        System.out.println("Liczba argumentow: " + getArgsLength("","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""));


        System.out.println("sum of empty: " + calcSum());
        System.out.println("sum of 1 = " + calcSum(1));
        System.out.println("sum of 1, 2 = " + calcSum(1, 2));
        System.out.println("sum of 1, 2, 3 = " + calcSum(1, 2, 3));

        printObject(new Object());
        printObject(new Integer(1));
        printObject("Jakis string");
        printObject(new Date(2019,7,20));
        printObject(new Integer(1), "Jakis string", new Date(2019,7,20));
    }

    private static int getArgsLength(String... args) {
        return args.length;
    }

    // napisz metode obliczajaca sume n liczb naturalnych calkowitych - calcSum
    private static int calcSum(int... liczby) {
        int result = 0;

        for (int i = 0; i < liczby.length; i++) {
            result += liczby[i];
        }

        return result;
    }




    // przyklad z klasa Object
    private static void printObject(Object... objects) {
        for (int i = 0; i < objects.length; i++) {

            if (objects[i] instanceof Date) {
                System.out.println(((Date) objects[i]).getInfo());
            } else {
                System.out.println(objects[i]);
            }

        }
    }


}
