package com.sda.javarzw7.wprowadzenie.week3.exceptions;

public class MathUtils {

    // n >= 0; if < then exception
    // n = 0; n! = 1
    // n = 1; n! = 1
    // n = 3; n! = 1 * 2 * 3
    public static long calcFactorial(int number) {

        // System.out.println("********************");
        // System.out.println("number = " + number);

        if (number < 0) {
            throw new WrongNumberException("Silnia moze byc liczona tylko dla wartosci wiekszych lub rownych 0");
        }

        if (number == 0) {
            // System.out.println("zwracam 1");
            return 1;
        } else {
            // System.out.println("zwracam " + number + " * calcFactorial(" + (number - 1) + ")");
            return number * calcFactorial(number - 1);
        }

    }

    public static long calcFactorialWithLoop(int number) {

        if (number < 0) {
            throw new WrongNumberException("Silnia moze byc liczona tylko dla wartosci wiekszych lub rownych 0");
        }

        if (number == 0 || number == 1) {
            return 1;
        } else {
            long result = 1;

            for (int i = number; i > 0; i--) {
                result = result * i;
            }

            return result;
        }

    }
}

// 3 * calcFactorial(2)
// 3 *               2 * calcFactorial(1)
// 3 *               2 *               1 * calcFactorial(0)
// 3 *               2 *               1 *               1


class WrongNumberException extends RuntimeException {
    public WrongNumberException(String msg) {
        super(msg);
    }
}