package com.sda.javarzw7.wprowadzenie.week3;

import com.sda.javarzw7.wprowadzenie.week3.generics.MyCustomStack;
import org.junit.jupiter.api.Test;

public class TestMyCustomStack {


    @Test
    void test_1() {
        MyCustomStack<String> stack = new MyCustomStack<>(4);
        stack.push("Darek");
        stack.push("Monika");
        stack.push("Jarek");
        stack.push("Oskar");
        stack.push("Łukasz");


        MyCustomStack<Integer> stackOfIntegers = new MyCustomStack<>(4);

        System.out.println(stack.getInfo());
    }
}
