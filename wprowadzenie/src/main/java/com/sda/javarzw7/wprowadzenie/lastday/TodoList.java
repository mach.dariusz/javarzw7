package com.sda.javarzw7.wprowadzenie.lastday;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class TodoList implements ITodoList {

    private final Map<Integer, Todo> todos = new HashMap<>();

    @Override
    public void add(String title, String description, Priority priority) {

        // key is our id for next Todo Object in list
        int key = this.todos.keySet().stream()
                .max((o1, o2) -> o1 - o2)
                .orElse(0) + 1;     // orElse zwroci 0, tylko wtedy, gdy nie bedzie nic w todos

        this.todos.put(key, new Todo(key, title, description, priority));

        // save to file
        FileService.saveTodoList("applicationTodoList.txt", this);
    }


    @Override
    public void remove(int id) {
        Todo removedTodo = this.todos.remove(id);
        if (removedTodo != null) {
            System.out.println("Usunieto obiekt Todo:\n" + removedTodo);
        } else {
            System.out.println("Nie ma obiektu w liscie z id = " + id);
        }

        // save to file
        FileService.saveTodoList("applicationTodoList.txt", this);
    }

    @Override
    public void markAsFinished(int id) {
        Todo todo = todos.get(id);
        todo.setFinished(true);

        this.todos.put(id, todo);

        // save to file
        FileService.saveTodoList("applicationTodoList.txt", this);
    }

    @Override
    public void show() {
        this.todos.entrySet().forEach(entry ->
                System.out.printf("Klucz: %d | Wartosc: %s", entry.getKey(), entry.getValue()));

        // below line does the same as above
        // this.todos.forEach((integer, todo) -> System.out.printf("Klucz: %d | Wartosc: %s\n", integer, todo));
    }

    public int getSize() {
        return this.todos.size();
    }

    public Todo getTodo(int id) {
        return this.todos.get(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TodoList todoList = (TodoList) o;
        return Objects.equals(todos, todoList.todos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(todos);
    }
}

