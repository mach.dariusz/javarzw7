package com.sda.javarzw7.wprowadzenie.week2.encapsulation;

public class Test {

    public static void main(String[] args) {
        example1();
        example2();
    }

    private static void example1() {
        Employee employee = new Employee(
                "Dariusz",
                "Mach",
                1234565789
        );

        Employee employee2 = new Employee(1234567890);
        employee2.setFirstName("Jan");
        employee2.setLastName("Kowalski");
    }

    private static void example2() {
        Human human = new Human("X","Y",Gender.MALE);
    }
}
