package com.sda.javarzw7.programowanie1.day1;

import java.util.Objects;

public class Test {

    public static void main(String[] args) {

        Animal animal = new Animal("Alfred", Type.BIRD);
        Animal bird = new Bird("Alfred", Type.BIRD);

        System.out.println(animal == bird);
        System.out.println(animal.equals(bird));
    }
}

class Animal {
    String name;
    Type type;

    public Animal(String name, Type type) {
        this.name = name;
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        Animal animal = (Animal) o;
        return Objects.equals(name, animal.name) &&
                Objects.equals(type, animal.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type);
    }
}

class Bird extends Animal {

    void foo(Object... o) {

    }

    void bar() {
        foo();
        foo(1);
        foo("A", new Employee("A"), 1);

    }

    int gatunek;

    public Bird(String name, Type type) {
        super(name, type);
    }
}


enum Type {
    DOG,
    CAT,
    BIRD
}

class B implements A {

    public static void main(String[] args) {
        System.out.println(A);
        // A = "A"; // Error:(62, 9) java: cannot assign a value to final variable A
    }
}

interface A {
    public static final String A = "A";
}