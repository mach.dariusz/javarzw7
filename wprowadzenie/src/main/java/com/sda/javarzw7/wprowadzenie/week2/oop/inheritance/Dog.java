package com.sda.javarzw7.wprowadzenie.week2.oop.inheritance;

public class Dog extends Animal {

    private String breed;

    public Dog(String name, String breed, int age) {
        super(name);
        this.breed = breed;
        this.setAge(age);
    }

    @Override
    public String toString() {
        return "imie psa: " + this.getName() + ", rasa: " + this.breed + ", wiek: " + this.getAge();
    }

    // Object -> Animal -> Dog
}
