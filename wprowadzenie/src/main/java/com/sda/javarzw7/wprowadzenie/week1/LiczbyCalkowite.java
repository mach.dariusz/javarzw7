package com.sda.javarzw7.wprowadzenie.week1;

public class LiczbyCalkowite {

    public static void main(String[] args) {
        System.out.println("\toperacjeNaIntach():");
        operacjeNaIntach();
        System.out.println("\n\toperacjeNaLongach():");
        operacjeNaLongach();
        
        System.out.println("\nWynik z getByteValue(false): " + getByteValue(false));
        System.out.println("\nWynik z getByteValue(true): " + getByteValue(true));
        System.out.println("\nWynik z getShortValue(false): " + getShortValue(false));
        System.out.println("\nWynik z getShortValue(true): " + getShortValue(true));

        int character = 'a';
        System.out.println("character: " + character);
     }

    /**
     @return max byte value if true, if false min byte value
     */
    private static byte getByteValue(boolean returnMinValue) {
        byte result;

        if (returnMinValue) {
            // return min value
            result = -128;
        } else {
            // return max value
            result = 127;
        }

        return result;
    }

    private static short getShortValue(boolean returnMinValue) {
        short result;

        if (returnMinValue) {
            // return min value
            result = -32768;
        } else {
            // return max value
            result = 32767;
        }

        return result;
    }

    private static void operacjeNaLongach() {
        long a = 1_000_000_000_000L;
        System.out.println("a=" + a);

        int b = (int) 10L;
        System.out.println("b=" + b);

        long c = a + b;
        System.out.println("c=" + c);
    }

    private static void operacjeNaIntach() {
        int a = 100;
        int b = 202;

        int c = a + b;

        System.out.println("a=" + a);
        System.out.println("b=" + b);
        
        System.out.println("c=" + a + b); // konkatenacja
        System.out.println("c=" + (a + b)); // najpierw operacja w nawiasie
        System.out.println("c=" + c);
    }
}