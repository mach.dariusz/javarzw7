package com.sda.javarzw7.wprowadzenie.week2.oop.polymorphism.animals;

public class Animal {

    private final String name;

    public Animal(String name) {
        this.name = name;
    }

    public void sound() {
        System.out.printf("My name is %s, I don't know what my sound is ;-(", this.name);
    }

    public String getName() {
        return this.name;
    }
}
