package com.sda.javarzw7.wprowadzenie.lastday;

import java.io.Serializable;
import java.util.Objects;

public class Todo implements Serializable {

    private final int id;
    private final String title;
    private final String description;
    private final Priority priority;
    private boolean finished;

    public Todo(int id, String title, String description, Priority priority) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.priority = priority;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Priority getPriority() {
        return priority;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Todo todo = (Todo) o;
        return id == todo.id && Objects.equals(title, todo.title) &&
                Objects.equals(description, todo.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description);
    }

    @Override
    public String toString() {

        return String.format("Todo id: %d \n\ttitle: %s \n\tdescription: %s \n\tpriority: %s \n\tfinished: %s\n",
                id, title, description, priority, finished ? "yes" : "no"
        );
    }
}

