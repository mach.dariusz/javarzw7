package com.sda.javarzw7.wprowadzenie.week2.oop.interfaces;

public class TestInterfaces {

    public static void main(String[] args) {

        Multiply m = new Multiply();
        double a = m.calculate(10);
        System.out.println(a);

        Divide n = new Divide();
        double b = n.calculate(10);
        System.out.println(b);
    }
}
