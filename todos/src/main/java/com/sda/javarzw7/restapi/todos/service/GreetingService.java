package com.sda.javarzw7.restapi.todos.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class GreetingService {
    private final AtomicLong counter = new AtomicLong();
    private final Map<String, Long> contentMap = new HashMap<>();
    /**
     *
     * @param content
     * @return -1 if content not found, if found return its request id
     */
    public long checkContent(String content) {

        return contentMap.containsKey(content) ? contentMap.get(content) : getNewId(content);
    }

    private long getNewId(String content) {
        long tempCounter = counter.incrementAndGet();
        contentMap.put(content, tempCounter);

        return tempCounter;
    }
}
