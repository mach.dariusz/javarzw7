package com.sda.javarzw7.wprowadzenie.week4.powtorka;

public class Wyjatki {

    public static void main(String[] args) throws MyCustomException { // to spowoduje blad aplikacji, jesli exception wystapi
        try { // to uchroni aplikacje przed crashem ...
            Wyjatki.foo();
        } catch (MyCustomException e) {
            System.out.println("Wystapil wyjatek MyCustomException ...");
        }

        System.out.println("AAA");
    }

    public static void foo() throws MyCustomException {
        int x = 0;

        if (x == 1) {
            throw new MyCustomRuntimeException("Tu przekazujemy wiadomosc o wyjatku ...");
        }

        if (x == 0) {
            throw new MyCustomException();
        }
    }
}


class MyCustomRuntimeException extends RuntimeException {
    public MyCustomRuntimeException(String msg) {
        super(msg);
    }
}

class MyCustomException extends Exception {}