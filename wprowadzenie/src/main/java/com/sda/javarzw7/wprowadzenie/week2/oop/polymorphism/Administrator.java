package com.sda.javarzw7.wprowadzenie.week2.oop.polymorphism;

public class Administrator extends Employee {

    private Access access;

    public Administrator(String firstName, String lastName) {
        super(firstName, lastName);
    }

    public Administrator(String firstName, String lastName, Department department) {
        super(firstName, lastName, department);
    }

    public Administrator(String firstName, String lastName, Department department, Access access) {
        this(firstName, lastName, department);
        this.access = access;
    }

    public Access getAccess() {
        return access;
    }

    public void setAccess(Access access) {
        this.access = access;
    }

    @Override
    public String toString() {
        return super.toString()
                + "\n\tAdministrator{" +
                "access=" + access +
                '}';
    }
}

enum Access {
    LOW,
    MEDIUM,
    HIGH
}