package com.sda.javarzw7.wprowadzenie.week2.reservation;

public class Date {

    public int year;
    public int month;
    public int day;

    public Date(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public String getInfo() {
        return this.year + "-" + this.month + "-" + this.day;
    }
}
