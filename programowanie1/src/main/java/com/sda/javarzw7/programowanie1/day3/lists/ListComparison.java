package com.sda.javarzw7.programowanie1.day3.lists;

import java.util.*;

public class ListComparison {

    public static void main(String[] args) {

        final int COUNT = 150_000;

        String[] items = new String[COUNT];
        for (int i = 0; i < items.length; i++) {
            items[i] = "String " + i;
        }

        List<String> itemsList = Arrays.asList(items);

        // ArrayList at index 0
        List<String> list = new ArrayList<>();
        list.addAll(itemsList);

        System.out.printf(
                "Time to insert %d items to the ArrayList at index 0: %,12.6f secs %n",
                COUNT,
                insert(list, itemsList, 0) / 1_000_000_000.0);

        // LinkedList at index 0
        list = new LinkedList<>();
        list.addAll(itemsList);

        System.out.printf(
                "Time to insert %d items to the LinkedList at index 0: %,12.6f secs %n",
                COUNT,
                insert(list, itemsList, 0) / 1_000_000_000.0);

        // ArrayList in the middle
        list = new ArrayList<>();
        list.addAll(itemsList);

        System.out.printf(
                "Time to insert %d items to the ArrayList in the middle: %,12.6f secs %n",
                COUNT,
                insert(list, itemsList, items.length / 2) / 1_000_000_000.0);

        // LinkedList in the middle
        list = new LinkedList<>();
        list.addAll(itemsList);

        System.out.printf(
                "Time to insert %d items to the LinkedList in the middle: %,12.6f secs %n",
                COUNT,
                insert(list, itemsList, items.length / 2) / 1_000_000_000.0);


        // ArrayList in the middle with iterator
        list = new ArrayList<>();
        list.addAll(itemsList);

        System.out.printf(
                "Time to insert %d items to the ArrayList in the middle: %,12.6f secs %n",
                COUNT,
                insertWithIterator(list, itemsList, items.length / 2) / 1_000_000_000.0);

        // LinkedList in the middle with iterator
        list = new LinkedList<>();
        list.addAll(itemsList);

        System.out.printf(
                "Time to insert %d items to the LinkedList in the middle: %,12.6f secs %n",
                COUNT,
                insertWithIterator(list, itemsList, items.length / 2) / 1_000_000_000.0);


    }

    private static long insert(List<String> list, List<String> items, int target) {
        long start = System.nanoTime();

        for (String item : items) {
            list.add(target, item);
        }

        long end = System.nanoTime();

        return end - start;
    }

    private static long insertWithIterator(List<String> list, List<String> items, int target) {
        long start = System.nanoTime();

        ListIterator<String> iterator = list.listIterator(target);

        for (String item : items) {
            iterator.add(item);
        }

        long end = System.nanoTime();

        return end - start;
    }


}
