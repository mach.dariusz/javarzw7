package com.sda.javarzw7.programowanie1.day2;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

public class Application {

    public static void main(String[] args) throws InterruptedException {
        // create LIFO
        // add 10 items (class -> PlytaBetonowa (wysokosc, szerokosc, waga))
        // handle all exceptions
        // if LIFO throw exception handle it and remove from LIFO, then try add again
        // print all state changes
        testLifo();

        // create FIFO ( <=6 -> 2 sec, <=11 -> 1,5 sec, > 11 -> 1,2 sec)
        // add 30 items (can be generated - class -> PostConsumer (id, name) )
        // handle exception -> add more 3 sec sleep and display msg about break
        // try to remove all at the end
        testFifo();
    }

    static void testFifo() throws InterruptedException {
        MyCustomQueue<PostConsumer> queue = new MyCustomQueue<>(10);

        for (int i = 1; i <= 30; i++) {
            addToFifo(queue, new PostConsumer(i, "Consumer " + i));
        }

        while (queue.getSize() > 0) {
            TimeUnit.SECONDS.sleep(1);
            System.out.println("Obsuguje osobe z kolejki:");
            System.out.println(queue.pop());
        }
    }

    private static void addToFifo(MyCustomQueue<PostConsumer> queue, PostConsumer consumer) throws InterruptedException {
        if (queue.getCapactity() <= 6) {
            TimeUnit.SECONDS.sleep(2);
        } else if (queue.getCapactity() <= 11) {
            TimeUnit.MILLISECONDS.sleep(1500);
        } else {
            TimeUnit.MILLISECONDS.sleep(1200);
        }

        try {
            System.out.println("Nowa osoba w kolejce:");
            System.out.println(consumer);
            queue.push(consumer);
        } catch (QueueIsFullException e) {
            System.out.println("Kolejka jest pelna, obsluguje klienta:");
            System.out.println(queue.pop());

            System.out.println("Ide na przerwe, zaraz wracam ...");
            TimeUnit.SECONDS.sleep(3);

            System.out.println("Wpuszczam nowa osobe do kolejki:");
            queue.push(consumer);
        }

    }

    static void testLifo() throws InterruptedException {

        MyCustomStack<PlytaBetonowa> stack = new MyCustomStack<>(7);

        addToLifo(stack, new PlytaBetonowa(10, 15, 2));
        addToLifo(stack, new PlytaBetonowa(11, 11, 3));
        addToLifo(stack, new PlytaBetonowa(12, 12, 4));
        addToLifo(stack, new PlytaBetonowa(13, 13, 5));
        addToLifo(stack, new PlytaBetonowa(14, 14, 6));
        addToLifo(stack, new PlytaBetonowa(15, 16, 7));
        addToLifo(stack, new PlytaBetonowa(16, 17, 8));
        addToLifo(stack, new PlytaBetonowa(17, 18, 9));
        addToLifo(stack, new PlytaBetonowa(18, 19, 10));
    }

    private static void addToLifo(MyCustomStack<PlytaBetonowa> stack, PlytaBetonowa plyta) throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);

        try {
            stack.push(plyta);
            System.out.println("Dodano nowa plyte:");
            System.out.println(plyta);
        } catch (StackIsFullException e) {
            System.out.println("Stos pelny, usunieto plyte:");
            System.out.println(stack.pop());

            TimeUnit.SECONDS.sleep(1);
            System.out.println("Dodaje jeszcze raz:");
            System.out.println(plyta);
            stack.push(plyta);
        }
    }
}

class PostConsumer {
    private int id;
    private String name;

    public PostConsumer(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "PostConsumer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

class PlytaBetonowa {

    private double width;
    private double height;
    private double weight;

    public PlytaBetonowa(double width, double height, double weight) {
        this.width = width;
        this.height = height;
        this.weight = weight;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return "PlytaBetonowa{" +
                "width=" + width +
                ", height=" + height +
                ", weight=" + weight +
                '}';
    }
}
