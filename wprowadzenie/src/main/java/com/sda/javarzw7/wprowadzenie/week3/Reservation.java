package com.sda.javarzw7.wprowadzenie.week3;

import java.util.Arrays;

public class Reservation {

    private Customer[] customers;
    private Trip trip;

    public Reservation(Trip trip, Customer[] customers) {
        this.trip = trip;
        this.customers = customers;
    }

    public Customer[] getCustomers() {
        return customers;
    }

    public Trip getTrip() {
        return trip;
    }

    @Override
    public String toString() {
        return "Reservation :" +
                "\n\t" + Arrays.toString(customers) +
                ",\n\t" + trip;
    }
}
