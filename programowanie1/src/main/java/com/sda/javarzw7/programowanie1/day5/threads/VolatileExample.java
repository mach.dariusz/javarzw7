package com.sda.javarzw7.programowanie1.day5.threads;

public class VolatileExample {

    private static volatile int MY_INT = 0;

    public static void main(String[] args) {
        new ChangeListener().start();
        new ChangeMaker().start();
    }

    static class ChangeListener extends Thread {
        @Override
        public void run() {
            System.out.println("Started ChangeListener() ...");
            int local_value = MY_INT;

            while(MY_INT < 5) {
                // System.out.println("MY_INT: " + MY_INT);
                if (local_value != MY_INT) {
                    System.out.println("Change for MY_INT -> " + MY_INT);
                    local_value = MY_INT;
                }
            }

            System.out.println("Finished ChangeListener() ...");
        }
    }

    static class ChangeMaker extends Thread {
        @Override
        public void run() {
            System.out.println("Started ChangeMaker() ...");
            int local_value = MY_INT;

            while (MY_INT < 5) {
                System.out.println("Incrementing MY_INT to -> " + (local_value + 1));
                MY_INT = ++local_value;

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            System.out.println("Finished ChangeMaker() ...");
        }
    }
}
