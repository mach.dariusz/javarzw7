package com.sda.javarzw7.wprowadzenie.week2.oop.interfaces;

public interface MyCustomInterface {

    /**
     *
     * @param object should be int or double
     */
    void printValue(Object object);

    double multiply(int x, int y);
    double multiply(double x, double y);

    double divide(int x, int y);
    double divide(double x, double y);

    double sum(double x, double y);

}
