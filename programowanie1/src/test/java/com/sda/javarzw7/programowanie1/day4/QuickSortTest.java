package com.sda.javarzw7.programowanie1.day4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class QuickSortTest {

    @Test
    void quickSort() {

        int[] array = {9, -3, 5, 2, 6, 8, -6, 1, 3};
        int[] sortedArray = {-6, -3, 1, 2, 3, 5, 6, 8, 9};

        QuickSort.sort(array, 0, array.length - 1);

        Assertions.assertArrayEquals(array, sortedArray);
    }


    @Test
    void sort_time() {
        int[] array = getRandomArray(1_000_000, 1000);
        int[] arrayToSort = array.clone();

        // System.out.println(Arrays.toString(array));

        LocalDateTime start = LocalDateTime.now();
        Arrays.sort(array);
        LocalDateTime end = LocalDateTime.now();

        System.out.println("quick sort: " + ChronoUnit.MILLIS.between(start, end));

        start = LocalDateTime.now();
        QuickSort.sort(arrayToSort, 0, arrayToSort.length - 1);
        end = LocalDateTime.now();

        System.out.println("custom quick sort: " + ChronoUnit.MILLIS.between(start, end));

        assertArrayEquals(array, arrayToSort);
    }


    private static int[] getRandomArray(int size, int range) {
        int[] array = new int[size];

        Random random = new Random();
        for (int i = 0; i < size; i++) {
            array[i] = random.nextInt(range);
        }

        return array;
    }
}