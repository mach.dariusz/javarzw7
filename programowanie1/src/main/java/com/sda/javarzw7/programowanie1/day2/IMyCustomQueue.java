package com.sda.javarzw7.programowanie1.day2;

public interface IMyCustomQueue<T> {

    /**
     * add object to queue
     */
    void push(T object);

    /**
     *
     * @return object from queue
     */
    T pop();

    /**
     *
     * @return object from queue, but without removing it
     */
    T peek();
}
