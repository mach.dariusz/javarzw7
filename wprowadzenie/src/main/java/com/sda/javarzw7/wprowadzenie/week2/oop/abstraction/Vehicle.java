package com.sda.javarzw7.wprowadzenie.week2.oop.abstraction;

public abstract class Vehicle {

    private String companyName;

    public Vehicle(String companyName) {
        this.companyName = companyName;
    }

    public abstract double getDistance();

    public abstract double getFuelUsage();

    public double getEfficiency() {
        return getFuelUsage() / getDistance();
    }

    @Override
    public String toString() {
        return "type: " + this.getClass().getName()
                + ", company name: " + this.companyName;
    }
}
