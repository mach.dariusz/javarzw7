package com.sda.javarzw7.programowanie1.day2;

public interface IMyCustomStack<T> {

    /**
     * add object to stack
     */
    void push(T object);

    /**
     *
     * @return object from stack
     */
    T pop();

    /**
     *
     * @return object from stack, but without removing it
     */
    T peek();
}
