package com.sda.javarzw7.programowanie1.day2.genericsExample;

public class Box<T> {

    T[] box;
    int head;
    int size;

    public Box(int capacity) {
        this.box = (T[]) new Object[capacity];
        this.head = 0;
        this.size = 0;
    }

    public void add(T object) {
        this.box[this.head] = object;
        this.head++;
        this.size++;
    }

    public T peek() {
        return this.box[head - 1];
    }

    public int getSize() {
        return this.size;
    }
}
