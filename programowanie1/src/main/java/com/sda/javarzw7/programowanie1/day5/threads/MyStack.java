package com.sda.javarzw7.programowanie1.day5.threads;

public class MyStack<E> implements IMyStack<E> {

    private E[] stack;
    private int head;
    private int size;

    public MyStack(int capacity) {
        this.stack = (E[]) new Object[capacity];
        this.head = 0;
        this.size = 0;
    }

    @Override
    public synchronized void push(E object) throws InterruptedException {
        while (isFull()) {
            System.out.println("Producer: waiting for place on shelf ...");
            wait();
        }

        this.stack[this.head] = object;
        this.head++;
        this.size++;

        notify();
    }

    @Override
    public synchronized E pop() throws InterruptedException {
        while (isEmpty()) {
            System.out.println("Consumer: waiting for products ...");
            wait();
        }

        E tempObject = this.stack[this.head - 1];
        this.stack[this.head - 1] = null;
        this.head--;
        this.size--;

        notify();

        return tempObject;
    }

    @Override
    public E peek() {
        return null;
    }

    public boolean isFull() {
        return this.size == stack.length;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }
}

interface IMyStack<E> {
    void push(E object) throws InterruptedException ;

    E pop() throws InterruptedException;

    E peek();
}
