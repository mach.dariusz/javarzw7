package com.sda.javarzw7.programowanie1.day5.threads;

import com.sda.javarzw7.programowanie1.day2.IMyCustomStack;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ShopApp {
    private final static IMyStack<Product> SHELF = new MyStack<>(10);

    public static void main(String[] args) {

        try {
            SHELF.push(ShopUtils.getRandomProduct());
            SHELF.push(ShopUtils.getRandomProduct());
            SHELF.push(ShopUtils.getRandomProduct());
            SHELF.push(ShopUtils.getRandomProduct());
            SHELF.push(ShopUtils.getRandomProduct());
            SHELF.push(ShopUtils.getRandomProduct());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Thread producer = new Thread(() -> {
            while (true) {
                try {
                    System.out.println("Producer: New supply ...");
                    Product product = ShopUtils.getRandomProduct();
                    SHELF.push(product);
                    System.out.println("Producer: Added: " + product);

                    TimeUnit.SECONDS.sleep(new Random().nextInt(60));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread consumer = new Thread(() -> {
            while (true) {
                try {
                    System.out.println("Consumer: I am going for a shopping ...");
                    Product product = SHELF.pop();
                    System.out.println("Consumer: I have bought " + product);
                    TimeUnit.SECONDS.sleep(new Random().nextInt(10));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        producer.start();
        consumer.start();
    }
}

class ShopUtils {

    private static Product[] products = {
            new Product("Milk"),
            new Product("Butter"),
            new Product("Nuts"),
            new Product("Pepsi"),
            new Product("Fanta"),
            new Product("Beer")
    };


    public static Product getRandomProduct() {
        Random random = new Random();

        return products[random.nextInt(products.length)];
    }
}

class Product {
    private String name;

    public Product(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                '}';
    }
}