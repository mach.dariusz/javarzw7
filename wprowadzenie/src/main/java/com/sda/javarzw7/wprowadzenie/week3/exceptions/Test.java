package com.sda.javarzw7.wprowadzenie.week3.exceptions;

public class Test {


    public static void main(String[] args) {


        String a = "A";
        String b = "B";
        String c = "C";

        System.out.println(b);
        try {
            b = "c";

            if (a.equals("A")) {
                throw new MyCustomException();
            }

            a = "1";
            b = "2";
            c = "3";
        } catch(MyCustomException e) {
            System.out.println("Wystapil MyCustomException()");
            b = "b";
            a = "a";
            c = "c";

            try {
                throw new Exception();
            } catch (Exception e1) {
                System.out.println("Exception in another try catch");
            } finally {
                System.out.println("finally in inner try catch");
            }
        } catch (Exception e) {
            System.out.println("Wystapil Exception()");
            b = "5";
            a = "4";
            c = "6";
        } finally {
            System.out.println("finally executed");
        }

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }
}

class MyCustomException extends Exception {

}