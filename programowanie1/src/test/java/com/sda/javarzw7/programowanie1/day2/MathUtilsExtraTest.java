package com.sda.javarzw7.programowanie1.day2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MathUtilsExtraTest {

    @Test
    void sum_test() {
        Assertions.assertEquals(
                MathUtilsExtra.sum(10,20),
                30
        );
    }
}
