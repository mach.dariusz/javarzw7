package com.sda.javarzw7.programowanie1.day6;

import java.util.Arrays;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        List integers = Arrays.asList(new Integer[]{1, 2, 3});
        List ints = Arrays.asList(new int[]{1, 2, 3});

        System.out.println(integers.size() == ints.size());
    }
}
