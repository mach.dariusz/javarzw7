package com.sda.javarzw7.wprowadzenie.week2;

public class Person {

    String firstName;
    String lastName;
    int age;

    // konstruktor defaultowy
    public Person() {}

    // konstruktor customowy przyjmujacy 2 argumenty
    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    // konstruktor customowy przyjmujacy 3 argumenty
    public Person(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
}
