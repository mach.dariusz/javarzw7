package com.sda.javarzw7.wprowadzenie.lastday;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TodoTest {

    @DisplayName("Create new Todo object")
    @Test
    void test_1() {
        Todo todo = new Todo(
                1,
                "Nauka Javy",
                "Moze warto wreszcie sie wziac do nauki, a nie tylko w weekend jak mi kaze Darek",
                Priority.HIGH);

        System.out.println(todo);

        assertEquals(todo.isFinished(), false);
    }
}