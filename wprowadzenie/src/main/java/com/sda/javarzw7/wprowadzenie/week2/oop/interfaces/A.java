package com.sda.javarzw7.wprowadzenie.week2.oop.interfaces;

public abstract class A implements MyInterface {
    abstract void foo();
}


abstract class B extends A {
    abstract void bar();
}

class C extends B {

    @Override
    void foo() {
        // TODO
    }

    @Override
    void bar() {
        // TODO
    }

    @Override
    public double calculate(double x) {
        // TODO
        return 0;
    }

}