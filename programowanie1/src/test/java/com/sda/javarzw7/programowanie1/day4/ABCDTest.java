package com.sda.javarzw7.programowanie1.day4;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ABCDTest {

    char[][] array = {
            {' ', ' ', ' ', ' ', ' ', 'D'}, // i = 0
            {' ', 'D', ' ', ' ', 'C', ' '}, // i = 1
            {' ', 'C', ' ', 'B', ' ', ' '}, // i = 2
            {' ', 'B', 'A', ' ', ' ', ' '}, // i = 3
            {' ', 'A', 'B', 'C', 'D', ' '}, // i = 4
            {' ', ' ', 'C', ' ', ' ', ' '}, // i = 5
            {' ', ' ', 'D', 'C', 'B', 'A'}, // i = 6
    }; // j:  0    1    2    3    4    5

    @Test
    void search() {

        List<Occurrence> list = new ArrayList<>();
        list.add(new Occurrence(new Point(4, 1), new Point(4, 4), Direction.EAST));
        list.add(new Occurrence(new Point(3, 2), new Point(0, 5), Direction.NORTHEAST));
        list.add(new Occurrence(new Point(6, 5), new Point(6, 2), Direction.WEST));
        list.add(new Occurrence(new Point(4, 1), new Point(1, 1), Direction.NORTH));
        list.add(new Occurrence(new Point(3, 2), new Point(6, 2), Direction.SOUTH));


        assertTrue(
                ABCD.search(array, "ABCD").containsAll(list)
        );
    }


    @Test
    void checkCharAt() {
        assertEquals(true, ABCD.checkCharAt(array, 'A', new Point(4, 1)));
    }

    @Test
    void checkAllOccurrencesOfSingleLetter() {

        List<Point> list = new ArrayList<>();
        list.add(new Point(3,2));
        list.add(new Point(4,1));
        list.add(new Point(6,5));

        assertEquals(
                ABCD.checkAllOccurrencesOfSingleLetter(array, 'A'),
                list
                );
    }

    @Test
    void searchWord() {

        Occurrence occurrence = ABCD.searchWord("ABCD", 0, Direction.EAST, array, new Point(4,1), new Point(4,1));
        Occurrence expectedOccurrence = new Occurrence(new Point(4,1), new Point(4,4), Direction.EAST);
        assertEquals(occurrence, expectedOccurrence);

        occurrence = ABCD.searchWord("ABCD", 0, Direction.WEST, array, new Point(6,5), new Point(6,5));
        expectedOccurrence = new Occurrence(new Point(6,5), new Point(6,2), Direction.WEST);
        assertEquals(occurrence, expectedOccurrence);

        occurrence = ABCD.searchWord("ABCD", 0, Direction.NORTH, array, new Point(4,1), new Point(4,1));
        expectedOccurrence = new Occurrence(new Point(4,1), new Point(1,1), Direction.NORTH);
        assertEquals(occurrence, expectedOccurrence);

        occurrence = ABCD.searchWord("ABCD", 0, Direction.SOUTH, array, new Point(3,2), new Point(3,2));
        expectedOccurrence = new Occurrence(new Point(3,2), new Point(6,2), Direction.SOUTH);
        assertEquals(occurrence, expectedOccurrence);

        occurrence = ABCD.searchWord("ABCD", 0, Direction.NORTHEAST, array, new Point(3,2), new Point(3,2));
        expectedOccurrence = new Occurrence(new Point(3,2), new Point(0,5), Direction.NORTHEAST);
        assertEquals(occurrence, expectedOccurrence);

    }

    @Test
    void bigTable() {

        char[][] array = ABCD.generateRandomArray(100,100, "DAREK");
        // ABCD.printArray(array);

        // ABCD.search(array, "DAREK");

        array = ABCD.generateRandomArray(10_000,10_000, "RZESZÓW M");
        // ABCD.printArray(array);

        ABCD.search(array, "RZESZÓW M");
    }
}