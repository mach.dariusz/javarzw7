package com.sda.javarzw7.nosql.repository;

import com.sda.javarzw7.nosql.model.Address;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AddressRepository extends MongoRepository<Address, String> {

}
