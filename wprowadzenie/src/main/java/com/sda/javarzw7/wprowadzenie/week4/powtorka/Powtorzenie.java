package com.sda.javarzw7.wprowadzenie.week4.powtorka;

public class Powtorzenie {

    public static void main(String[] args) {
        Animal animal = new Animal("Alfred", AnimalType.CAT);
        String animalName = animal.getName();

        System.out.println("Imie mojego zwierzaka to: " + animalName
                + ", a wiek to: " + AnimalUtils.calcAge(animal));

    }
}

class AnimalUtils {

    int id = 100;

    public static int calcAge(Animal animal) {
        if (animal.getAnimalType() == AnimalType.CAT) {
            return 25;
        }

        // System.out.println(id); // error here
        return 10;
    }
}

class Animal {

    static int id = 1000;

    private AnimalType animalType;
    private String name;

    public Animal(String name, AnimalType animalType) {
        this.animalType = animalType;
        this.name = name;

        System.out.println(id); // good here
    }

    public String getName() {
        return name;
    }

    public AnimalType getAnimalType() {
        return animalType;
    }
}

enum AnimalType {
    DOG,
    CAT
}