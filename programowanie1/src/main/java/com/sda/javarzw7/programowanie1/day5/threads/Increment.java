package com.sda.javarzw7.programowanie1.day5.threads;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

public class Increment {

    static int counter = 0;

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                increment();
            }

        });
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                increment();
            }

        });
        Thread t3 = new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                increment();
            }

        });
        t1.start();
        t2.start();
        t3.start();

        // TimeUnit.SECONDS.sleep(1); // sleep for 1 sec and run again
        // t1.join(); // wait for t1 thread to complete and die

        TimeUnit.SECONDS.sleep(2);
        System.out.println(t1.getState());
        System.out.println(t2.getState());
        System.out.println(t3.getState());
        System.out.println(counter);
    }

    static synchronized void increment() {
        counter++;
    }
}
