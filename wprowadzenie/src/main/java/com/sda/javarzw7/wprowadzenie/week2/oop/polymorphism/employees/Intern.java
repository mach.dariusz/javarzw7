package com.sda.javarzw7.wprowadzenie.week2.oop.polymorphism.employees;

public class Intern extends Employee {

    public Intern(String firstName, String lastName) {
        super(firstName, lastName, Type.INTERN);
    }
}
