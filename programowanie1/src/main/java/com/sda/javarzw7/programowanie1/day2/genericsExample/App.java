package com.sda.javarzw7.programowanie1.day2.genericsExample;

public class App {

    public static void main(String[] args) {
        Box<Figure> box = new Box<>(5);
        box.add(new Rectangle());
        box.add(new Square());
        box.add(new Circle());
        System.out.println(box.getSize());

        System.out.println(box.peek().getName());

        Box<Integer> integerBox = new Box<>(2);
    }
}
