package com.sda.javarzw7.nosql.util;

import com.sda.javarzw7.nosql.model.Address;
import com.sda.javarzw7.nosql.model.Department;
import com.sda.javarzw7.nosql.model.Employee;
import com.sda.javarzw7.nosql.model.Salary;
import com.sda.javarzw7.nosql.repository.AddressRepository;
import com.sda.javarzw7.nosql.repository.EmployeeRepository;
import com.sda.javarzw7.nosql.repository.SalaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Init implements CommandLineRunner {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private SalaryRepository salaryRepository;

    @Override
    public void run(String... args) throws Exception {

        employeeRepository.deleteAll();
        addressRepository.deleteAll();
        salaryRepository.deleteAll();

        // create salary object
        Salary salary10k = salaryRepository.save(new Salary(10_000.00));
        Salary salary20k = salaryRepository.save(new Salary(20_000.00));

        // create address object
        Address rzeszow = addressRepository.save(new Address("Reszów", "Lubelska", "10", "1A", "35-233", "Poland"));
        Address lancut = addressRepository.save(new Address("Łańcut", "Mickiewicza", "1B", "12", "32-200", "Poland"));

        // create employee object
        Employee emp1 = employeeRepository.save(new Employee("Mariusz", "Dach", rzeszow.getId(), salary10k.getId(), Department.HR));
        Employee emp2 = employeeRepository.save(new Employee("Jan", "Kowalski", lancut.getId(), salary20k.getId(), Department.IT));

        // read from db and print
        System.out.println(employeeRepository.findByLastNameAndFirstName("Dach", "Mariusz"));
    }
}
