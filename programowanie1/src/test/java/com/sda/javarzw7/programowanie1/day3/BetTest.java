package com.sda.javarzw7.programowanie1.day3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BetTest {

    @Test
    void calcSteps() {
        assertEquals(Bet.calcSteps(500_000, 1, 1_000_000), 1);
        assertEquals(Bet.calcSteps(24, 1, 1_000_000), 19);
        assertEquals(Bet.calcSteps(999_999, 1, 1_000_000), 19);
    }

    @Test
    void calcStepsRecur() {
        assertEquals(Bet.calcStepsRecur(500_000, 1, 1_000_000, 1), 1);
        assertEquals(Bet.calcStepsRecur(24, 1, 1_000_000, 1), 19);
        assertEquals(Bet.calcStepsRecur(999_999, 1, 1_000_000, 1), 19);
    }

    @Test
    void calcLogN() {
        assertEquals(Bet.calcLogN(1_000_000), 20);
    }
}