import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Todo } from './models/todo';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  private todosUrl = 'http://localhost:7777/api/v1/todos';

  constructor(private http: HttpClient) { }

  getAllTodos(): Observable<Array<Todo>> {

    return this.http.get<Array<Todo>>(this.todosUrl);
  }

  markAsFinish(id: number): Observable<Todo> {

    return this.http.patch<Todo>(this.todosUrl + '/' + id, null);
  }
}
