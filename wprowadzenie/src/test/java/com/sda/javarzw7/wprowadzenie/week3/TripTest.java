package com.sda.javarzw7.wprowadzenie.week3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import com.sda.javarzw7.wprowadzenie.week3.DateUtils;
import com.sda.javarzw7.wprowadzenie.week3.Trip;
import com.sda.javarzw7.wprowadzenie.week3.AbroadTrip;
import com.sda.javarzw7.wprowadzenie.week3.DomesticTrip;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Date;
import java.util.GregorianCalendar;

public class TripTest {

    @Test
    void test() {
        LocalDate startLocalDate = LocalDate.of(2019, 8, 5);
        LocalDate endLocalDate = LocalDate.of(2019, Month.AUGUST, 15);

        System.out.println(startLocalDate);
        System.out.println(endLocalDate);

        // look at the month
        Date date = new GregorianCalendar(2019, 1, 12).getTime();
        System.out.println(date);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        System.out.println(formatter.format(date));
    }

    @DisplayName("Test trip price")
    @Test
    void testTrip_1() throws ParseException {

        LocalDate startLocalDate = LocalDate.of(2019, 8, 5);
        LocalDate endLocalDate = LocalDate.of(2019, Month.AUGUST, 15);

        Date start = DateUtils.getDate(startLocalDate);
        Date end = DateUtils.getDate(endLocalDate.toString());

        Trip trip = new Trip(start, end, "Italy", 5000);

        Assertions.assertEquals(trip.getPrice(), 5000);
    }

    @DisplayName("Test abroad trip price")
    @Test
    void testTrip_2() throws ParseException {

        LocalDate startLocalDate = LocalDate.of(2019, 8, 5);
        LocalDate endLocalDate = LocalDate.of(2019, Month.AUGUST, 15);

        Date start = DateUtils.getDate(startLocalDate);
        Date end = DateUtils.getDate(endLocalDate.toString());

        Trip trip = new AbroadTrip(start, end, "Italy", 5000, 500);

        Assertions.assertEquals(trip.getPrice(), 5500);
    }

    @DisplayName("Test domestic trip price")
    @Test
    void testTrip_3() throws ParseException {

        LocalDate startLocalDate = LocalDate.of(2019, 8, 5);
        LocalDate endLocalDate = LocalDate.of(2019, Month.AUGUST, 15);

        Date start = DateUtils.getDate(startLocalDate);
        Date end = DateUtils.getDate(endLocalDate.toString());

        Trip trip = new DomesticTrip(start, end, "Italy", 5000, 10);

        Assertions.assertEquals(trip.getPrice(), 4500);
    }
}
