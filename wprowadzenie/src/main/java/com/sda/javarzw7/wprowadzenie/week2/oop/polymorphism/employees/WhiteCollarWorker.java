package com.sda.javarzw7.wprowadzenie.week2.oop.polymorphism.employees;

public class WhiteCollarWorker extends Employee {

    public WhiteCollarWorker(String firstName, String lastName) {
        super(firstName, lastName, Type.WHITE_COLLAR_WORKER);
    }
}
