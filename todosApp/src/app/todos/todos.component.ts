import { Component, OnInit } from '@angular/core';
import { TodosService } from '../todos.service';
import { Todo } from '../models/todo';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {

  todos: Array<Todo>;

  constructor(private todosService: TodosService) { }

  ngOnInit() {
    console.log('ngOnInit() in TodosComponent');

    this.findAll();
  }

  findAll() {
    this.todosService.getAllTodos().subscribe(result => {
      console.log('result:', result);
      this.todos = result;
    });
  }

  markFinish(id: number) {
    console.log('clicked markFinish on id: ' + id);
    this.todosService.markAsFinish(id).subscribe(result => {
      console.log(result);
      this.todos = this.todos.map(todo => {
        if (todo.id === id) {
          todo.finished = true;

          return todo;
        } else {
          return todo;
        }
      });
    });
  }

  remove(id: number) {
    console.log('clicked markFinish on id: ' + id);

  }
}
