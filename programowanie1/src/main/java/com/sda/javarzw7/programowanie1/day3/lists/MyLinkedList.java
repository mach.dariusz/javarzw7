package com.sda.javarzw7.programowanie1.day3.lists;

public class MyLinkedList<T> {

    private Node head;
    private int size;

    public MyLinkedList() {
        this.head = null;
        this.size = 0;
    }

    public void insert(T data) {
        Node node = new Node(data);
        node.setNext(this.head);
        this.head = node;
        this.size++;

        System.out.println("Added new object: " + node);
    }

    public boolean remove(T object) {
        // przypisujemy pierwszy obiekt z listy
        Node currentNode = this.head;
        Node previousNode = null;

        while (currentNode != null) {

            System.out.println("node: " + currentNode);
            if (currentNode.getData().equals(object)) {
                // przepisujemy referencje do nastepnego obiektu
                previousNode.setNext(currentNode.getNext());
                this.size--;

                return true;
            }

            // tu przypisujemy nastepny obiekt z listy,
            // jesli nie ma wiecej to bedzie null i sie zakonczy petla
            previousNode = currentNode;
            currentNode = currentNode.getNext();
        }

        return false;
    }

    public int getSize() {
        return this.size;
    }
}

class Node<T> {

    private T data;
    private Node next;

    public Node(T data) {
        this.data = data;
        this.next = null;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getNext() {
        return this.next;
    }

    public T getData() {
        return this.data;
    }

    @Override
    public String toString() {
        return String.format("Node[data: %s]", this.data, this.next);
    }
}
