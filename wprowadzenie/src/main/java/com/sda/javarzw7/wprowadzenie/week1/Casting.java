package com.sda.javarzw7.wprowadzenie.week1;

public class Casting {

    public static void main(String[] args) {
        
        Object objectWithString = "nasz string";
        System.out.println(objectWithString);
        
        String a = (String) objectWithString;
        System.out.println(a);
    }

}

