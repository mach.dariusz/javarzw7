package com.sda.javarzw7.wprowadzenie.week2.oop.polymorphism.employees;

public class TestEmployees {

    public static void main(String[] args) {
        example1();
        System.out.println();
        System.out.println("-----------------");
        System.out.println();
        example2();
    }

    private static void example1() {
        Employee employee = new Employee("Dariusz", "Mach");
        System.out.println(employee);

        Employee intern = new Intern("Jan", "Kowalski");
        System.out.println(intern);

        Employee manager = new Manager("Lidia","Wals");
        System.out.println(manager);

        Employee worker = new Worker("Janusz", "Narodu");
        System.out.println(worker);

        Employee whiteCollarWorker = new WhiteCollarWorker("Magda", "Gessler");
        System.out.println(whiteCollarWorker);
    }

    private static void example2() {

        Employee[] employees = {
                new Employee("Dariusz", "Mach"),
                new Intern("Jan", "Kowalski"),
                new Manager("Lidia","Wals"),
                new Worker("Janusz", "Narodu"),
                new WhiteCollarWorker("Magda", "Gessler")
        };

        for (Employee employee: employees) {
            System.out.println(employee);
        }

    }
}
