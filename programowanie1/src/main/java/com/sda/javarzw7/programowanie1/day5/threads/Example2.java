package com.sda.javarzw7.programowanie1.day5.threads;

public class Example2{

    public static void main(String[] args) {
        System.out.println("Name of executed thread: " + Thread.currentThread().getName());

        Thread t1 = new MyCustomThread2();
        Thread t2 = new MyCustomThread2();
        Thread t3 = new MyCustomThread2();
        Thread t4 = new MyCustomThread2();

        t1.start();
        t2.start();
        t3.start();
        t4.start();

    }
}


class MyCustomThread2 extends Thread {

    @Override
    public void run() {
        System.out.println("Name of executed thread: " + Thread.currentThread().getName());
    }
}