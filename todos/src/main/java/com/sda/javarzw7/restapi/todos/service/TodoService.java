package com.sda.javarzw7.restapi.todos.service;

import com.sda.javarzw7.restapi.todos.model.Todo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class TodoService {

    private final Map<Long, Todo> todos = new HashMap<>();
    private final AtomicLong todosCount = new AtomicLong();

    public List<Todo> getAll() {

        return new ArrayList<>(todos.values());
    }

    /**
     *
     * @param id
     * @return null if todo not found, otherwise todo
     */
    public Todo get(long id) {

       return todos.get(id);
    }


    public Todo add(String content) {

        Todo todo = new Todo(todosCount.incrementAndGet(), content);
        todos.put(todo.getId(), todo);

        return todo;
    }

    public Todo remove(long id) {

        return todos.remove(id);
    }

    public Todo markAsFinished(long id) {

        if (todos.containsKey(id)) {
            Todo todo = todos.get(id);
            todo.setFinished(true);

            todos.put(id, todo);

            return todos.get(id);
        } else {
            return null;
        }
    }
}
