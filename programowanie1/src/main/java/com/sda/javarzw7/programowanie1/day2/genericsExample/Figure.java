package com.sda.javarzw7.programowanie1.day2.genericsExample;

public interface Figure {

    String getName();
}
