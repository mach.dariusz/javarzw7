package com.sda.javarzw7.programowanie1.day2;

public class MathUtilsExtra {

    /**
     *
     * @param x
     * @param y
     * @return sum of x and y
     */
    public static long sum(int x, int y) {
        return x + y;
    }
}
