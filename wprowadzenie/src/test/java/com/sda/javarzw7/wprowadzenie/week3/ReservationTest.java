package com.sda.javarzw7.wprowadzenie.week3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;

public class ReservationTest {

    @DisplayName("Test for single customer and abroad trip")
    @Test
    void test_1() {

        Trip trip = new AbroadTrip(
                DateUtils.getDate(LocalDate.of(2019, Month.AUGUST, 1)),
                DateUtils.getDate(LocalDate.of(2019, Month.SEPTEMBER, 20)),
                "USA",
                6500,
                350
        );

        Customer customer = new Customer(1, "Jan", "Kowalski");

        // Customer[] customers = { customer }; // 1st way

        Reservation reservation = new Reservation(trip, new Customer[]{customer}); // second way
        System.out.println(reservation);
        Assertions.assertEquals(reservation.getTrip().getPrice(), 6850);
    }

    @DisplayName("Test for multiple customers and domestic trip")
    @Test
    void test_2() {

        Trip trip = new AbroadTrip(
                DateUtils.getDate(LocalDate.of(2019, Month.AUGUST, 1)),
                DateUtils.getDate(LocalDate.of(2019, Month.SEPTEMBER, 20)),
                "USA",
                6500,
                350
        );

        Customer customer_1 = new Customer(1, "Jan", "Kowalski");
        Customer customer_2 = new Customer(2, "Maria", "Styczynska");

        Customer[] customers = { customer_1, customer_2 };
        Reservation reservation = new Reservation(trip, customers);

        Assertions.assertEquals(reservation.getCustomers().length, 2);

        Assertions.assertEquals(
                reservation.getTrip().getPrice() * reservation.getCustomers().length,
                13700
        );

    }
}
