package com.sda.javarzw7.programowanie1.day5.threads;

import java.util.concurrent.TimeUnit;

public class Example4 {

    public static void main(String[] args) {
        Thread t1 = new Thread(new Printer());
        Thread t2 = new Thread(new Printer());
        Thread t3 = new Thread(new Printer());
        Thread t4 = new Thread(new Printer());

        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}


class Printer implements Runnable {

    @Override
    public void run() {
        System.out.println("Thread name: " + Thread.currentThread().getName());


        try {

            TimeUnit.SECONDS.sleep(2);
            System.out.println("Thread " + Thread.currentThread().getName() +  " run again ...");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}