package com.sda.javarzw7.wprowadzenie.week2.oop.interfaces;

public class Multiply implements MyInterface {

    @Override
    public double calculate(double x) {
        return x * 2;
    }
}

