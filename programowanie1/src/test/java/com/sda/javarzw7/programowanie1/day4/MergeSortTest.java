package com.sda.javarzw7.programowanie1.day4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MergeSortTest {

    @Test
    void sort() {
        int[] array = {5, 1, 6, 2, 3, 4};
        MergeSort.sort(array, array.length);

        assertArrayEquals(array, new int[]{1, 2, 3, 4, 5, 6});
    }
}