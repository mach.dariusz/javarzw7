package com.sda.javarzw7.wprowadzenie.week4.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Zadania {

    /*

    Napisz rozwiazania do ponizszych zadan przy uzyciu streamow i bez nich

    1. Wypisz wszystkich pracownikow z listy
    2. Wypisz tylko pracownikow, ktorych nazwisko konczy się na litere t
    3. Wypisz wszystkie numery id pracownikow
    4. Przeksztalc liste pracownikow, na liste stringow w formacie id: 1 -> nazwisko, np. id: 19 -> Mach


    5. Zwroc liste pracownikow, ktorych nazwisko i imie jest podzielne przez 2 (liczba znakow lacznie), wydrukuj
    6. Zwroc liste pracownikow, ktorych:
     * id bedzie w formacie 2019/SDA/id, gdzie id jest id pracownika, np 1,
     * salary bedzie wyliczane na podstawie wzoru: (dlugosc nazwiska * 100) - (dlugosc imienia * 35)
     * type bedzie dla id <1,3> -> MANAGER, id <4,8> -> WORKER, id 9 i wiecej -> WHITE_COLLAR_WORER
        na potrzeby tego zadania dodaj klase Employee2019 i zmodyfikuj na podstawie klasy Employee (bez dziedziczenia)
     */

    private static List<Employee> employeeList = new ArrayList<>();

    private static void init() {
        employeeList.add(new Employee(1, "Oskar", "Zaremba"));
        employeeList.add(new Employee(2, "Łukasz", "Możdżeń"));
        employeeList.add(new Employee(3, "Monika", "Inglot"));
        employeeList.add(new Employee(4, "Jaromir", "Kielar"));
        employeeList.add(new Employee(5, "Katarzyna", "Grata-Bugaj"));
        employeeList.add(new Employee(6, "Kamil", "Piękoś"));
        employeeList.add(new Employee(7, "Łukasz", "Brudniak"));
        employeeList.add(new Employee(8, "Piotr", "Baran"));
        employeeList.add(new Employee(9, "Małgorzata", "Baran"));
        employeeList.add(new Employee(10, "Paweł", "Skóra"));
    }

    public static void main(String[] args) {

        init();
        // 1. Wypisz wszystkich pracownikow z listy
        System.out.println("\nzadanie 1");
        zadanie_1a();
        System.out.println("**************************");
        zadanie_1b();

        // 2. Wypisz tylko pracownikow, ktorych nazwisko konczy się na litere t
        System.out.println("\nzadanie 2");
        zadanie_2a();
        System.out.println("**************************");
        zadanie_2b();

        // 3. Wypisz wszystkie numery id pracownikow
        System.out.println("\nzadanie 3");
        zadanie_3a();
        System.out.println("**************************");
        zadanie_3b();

        // 4. Przeksztalc liste pracownikow, na liste stringow w formacie id: 1 -> nazwisko, np. id: 19 -> Mach
        System.out.println("\nzadanie 4");
        zadanie_4a();
        System.out.println("**************************");
        zadanie_4b();

        // 5. Zwroc liste pracownikow, ktorych nazwisko i imie jest podzielne przez 2 (liczba znakow lacznie), wydrukuj
        System.out.println("\nzadanie 5");
        zadanie_5a();
        System.out.println("**************************");
        zadanie_5b();

        /*
        6. Zwroc liste pracownikow, ktorych:
                * id        -> bedzie w formacie 2019/SDA/id, gdzie id jest id pracownika, np 1,
                * salary    -> bedzie wyliczane na podstawie wzoru: (dlugosc nazwiska * 100) - (dlugosc imienia * 35)
                * type      -> bedzie dla id <1,3> -> MANAGER, id <4,8> -> WORKER, id 9 i wiecej -> WHITE_COLLAR_WORER

            na potrzeby tego zadania dodaj klase Employee2019
            i zmodyfikuj na podstawie klasy Employee (bez dziedziczenia)
         */
        System.out.println("\nzadanie 6");
        zadanie_6a();
        System.out.println("**************************");
        zadanie_6b();
    }

    private static void zadanie_1a() {
        for (int i = 0; i < employeeList.size(); i++) {
            System.out.println(employeeList.get(i));
        }
    }

    private static void zadanie_1b() {
        employeeList.forEach(System.out::println);
    }

    private static void zadanie_2a() {
        for (Employee employee : employeeList) {
            if (employee.getLastName().charAt(employee.getLastName().length() - 1) == 't') {
                System.out.println(employee);
            }
        }
    }

    private static void zadanie_2b() {
        employeeList.stream()
                .filter(e -> e.getLastName().charAt(e.getLastName().length() - 1) == 't')
                .forEach(System.out::println);
    }

    private static void zadanie_3a() {
        for (Employee employee : employeeList) {
            System.out.println(employee.getId());
        }
    }

    private static void zadanie_3b() {
        employeeList.stream().map(Employee::getId)
                .forEach(System.out::println);
    }

    private static void zadanie_4a() {
        List<String> listOfStrings = new ArrayList<>();
        for (Employee employee : employeeList) {
            listOfStrings.add(String.format("id: %d -> %s", employee.getId(), employee.getLastName()));
        }

        for (String res : listOfStrings) {
            System.out.println(res);
        }
    }

    private static void zadanie_4b() {
        List<String> listOfStrings = employeeList.stream()
                .map(employee -> String.format("id: %d -> %s", employee.getId(), employee.getLastName()))
                .collect(Collectors.toList());

        listOfStrings.forEach(System.out::println);
    }

    private static void zadanie_5a() {
        List<Employee> listOfEmps = new ArrayList<>();
        for (Employee employee : employeeList) {
            int nameLength = employee.getLastName().length() + employee.getFirstName().length();
            if (nameLength % 2 == 0) {
                listOfEmps.add(employee);
            }
        }

        for (Employee emp : listOfEmps) {
            System.out.println(emp);
        }
    }

    private static void zadanie_5b() {
        List<Employee> list = employeeList.stream()
                .filter(emp -> (emp.getLastName().length() + emp.getFirstName().length()) % 2 == 0)
                .collect(Collectors.toList());

        list.forEach(System.out::println);
    }

    /*
    6. Zwroc liste pracownikow, ktorych:
            * id        -> bedzie w formacie 2019/SDA/id, gdzie id jest id pracownika, np 1,
            * salary    -> bedzie wyliczane na podstawie wzoru: (dlugosc nazwiska * 100) - (dlugosc imienia * 35)
            * type      -> bedzie dla id <1,3> -> MANAGER, id <4,8> -> WORKER, id 9 i wiecej -> WHITE_COLLAR_WORER

        na potrzeby tego zadania dodaj klase Employee2019
        i zmodyfikuj na podstawie klasy Employee (bez dziedziczenia)
     */

    private static void zadanie_6a() {
        List<Employee2019> listOfNewEmployees = new ArrayList<>();

        for (Employee employee : employeeList) {


            listOfNewEmployees.add(
                    new Employee2019(
                            "2019/SDA/" + employee.getId(),
                            employee.getFirstName(),
                            employee.getLastName(),
                            getEmployeeType(employee),
                            getSalary(employee)
                    )
            );
        }

        for (Employee2019 employee2019: listOfNewEmployees) {
            System.out.println(employee2019);
        }
    }

    private static void zadanie_6b() {
        List<Employee2019> listOfNewEmployees = employeeList.stream().map(employee -> new Employee2019(
                "2019/SDA/" + employee.getId(),
                employee.getFirstName(),
                employee.getLastName(),
                getEmployeeType(employee),
                getSalary(employee))).collect(Collectors.toList());

        listOfNewEmployees.forEach(System.out::println);
    }

    private static double getSalary(Employee employee) {
        return  (employee.getLastName().length() * 100) - (employee.getFirstName().length() * 35);
    }

    private static EmployeeType getEmployeeType(Employee employee) {

        switch (employee.getId()) {
            case 1:
            case 2:
            case 3:
                return EmployeeType.MANAGER;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                return EmployeeType.WORKER;

            default:
                return EmployeeType.WHITE_COLLAR_WORER;
        }

    }

}

class Employee2019 {
    private String id;
    private String firstName;
    private String lastName;
    private EmployeeType type;
    private double salary;

    public Employee2019(String id, String firstName, String lastName, EmployeeType type, double salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.type = type;
        this.salary = salary;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public EmployeeType getType() {
        return type;
    }

    public void setType(EmployeeType type) {
        this.type = type;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee2019 that = (Employee2019) o;
        return Double.compare(that.salary, salary) == 0 &&
                Objects.equals(id, that.id) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, type, salary);
    }

    @Override
    public String toString() {
        return "Employee2019{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", type=" + type +
                ", salary=" + salary +
                '}';
    }
}

enum EmployeeType {
    MANAGER,
    WORKER,
    WHITE_COLLAR_WORER
}
