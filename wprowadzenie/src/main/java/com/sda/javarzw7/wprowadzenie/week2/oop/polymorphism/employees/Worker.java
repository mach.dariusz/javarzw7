package com.sda.javarzw7.wprowadzenie.week2.oop.polymorphism.employees;

public class Worker extends Employee {

    public Worker(String firstName, String lastName) {
        super(firstName, lastName, Type.WORKER);
    }
}
