package com.sda.javarzw7.programowanie1.day5.threads;

public class Example1 {


    public static void main(String[] args) {
        System.out.println("Name of executed thread: " + Thread.currentThread().getName());

        MyCustomThread customThread = new MyCustomThread();

        Thread t1 = new Thread(customThread);
        Thread t2 = new Thread(customThread);
        Thread t3 = new Thread(customThread);
        Thread t4 = new Thread(customThread);
        Thread t5 = new Thread(customThread);
        Thread t6 = new Thread(customThread);

        t1.start(); // start() -> run thread in new thread
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();


        for (int i = 0; i < 30; i++) {
            Thread thread = new Thread(customThread);
            thread.run(); // run() -> run thread in current thread
        }

    }
}


class MyCustomThread implements Runnable {

    @Override
    public void run() {
        System.out.println("Name of executed thread: " + Thread.currentThread().getName());
    }
}


