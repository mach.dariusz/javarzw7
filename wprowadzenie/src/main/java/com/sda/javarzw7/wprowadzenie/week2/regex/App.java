package com.sda.javarzw7.wprowadzenie.week2.regex;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App {
    public static void main(String[] args) {

        example1();
        example2();
        example3();
        example4();
        example5();
    }

    private static void example1() {
        Pattern pattern = Pattern.compile("ko*tek");
        Matcher matcher1 = pattern.matcher("koootek");
        Matcher matcher2 = pattern.matcher("ktek");
        Matcher matcher3 = pattern.matcher("koteczek");
        System.out.println("koootek: " + matcher1.matches());
        System.out.println("ktek: " + matcher2.matches());
        System.out.println("koteczek: " + matcher3.matches());
    }

    private static void example2() {
        Pattern pattern = Pattern.compile(",\\s+");
        String[] result = pattern.split("one,two, three four ,five");
        System.out.println(result.length);
        System.out.println(Arrays.toString(result));
        for (String s: result) {
            System.out.println(s);
        }
    }

    private static void example3() {
        Pattern pattern = Pattern.compile(",\\s*");
        String[] result = pattern.split("one,two, three four ,five");
        System.out.println(result.length);
        System.out.println(Arrays.toString(result));
        for (String s: result) {
            System.out.println(s);
        }
    }

    private static void example4() {
        Pattern pattern = Pattern.compile("[,*\\s*]");
        String[] result = pattern.split("one,two, three four ,five");
        System.out.println(result.length);
        System.out.println(Arrays.toString(result));
        for (String s: result) {
            System.out.println(s);
        }
    }

    private static void example5() {
        String wyraz = "one,two, three four ,five";
        String[] result = wyraz.split(",\\s*");
        System.out.println(result.length);
        System.out.println(Arrays.toString(result));
        for (String s: result) {
            System.out.println(s);
        }
        // to samo co powyzej
        for (int i = 0; i< result.length; i++) {
            System.out.println(result[i]);
        }
    }
}
