package com.sda.javarzw7.programowanie1.day5.threads;

public class Example3 {



    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName());

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        });

        Thread t2 = new Thread(() -> System.out.println(Thread.currentThread().getName()));

        t1.start();
        t2.start();
    }
}
