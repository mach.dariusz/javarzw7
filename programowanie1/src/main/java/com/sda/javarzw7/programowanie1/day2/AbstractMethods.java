package com.sda.javarzw7.programowanie1.day2;

public abstract class AbstractMethods {

    abstract double calcFuel();
    abstract double calcEfficiency();

    double calc() {
        return calcFuel() / calcEfficiency();
    }
}


class AM extends AbstractMethods {

    @Override
    double calcFuel() {
        return 0;
    }

    @Override
    double calcEfficiency() {
        return 0;
    }
}