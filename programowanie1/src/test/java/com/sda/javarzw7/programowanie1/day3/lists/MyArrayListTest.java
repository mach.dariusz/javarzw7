package com.sda.javarzw7.programowanie1.day3.lists;

import org.junit.jupiter.api.Test;

import java.rmi.NoSuchObjectException;

import static org.junit.jupiter.api.Assertions.*;

class MyArrayListTest {

    @Test
    void addTest() {
        MyArrayList<String> list = new MyArrayList<>();

        list.add("String 1");
        list.add("String 2");
        list.add("String 3");

        assertEquals(list.getSize(), 3);
    }

    @Test
    void removeTest() {
        MyArrayList<String> list = new MyArrayList<>();

        list.add("String 1");
        list.add("String 2");
        list.add("String 3");

        assertEquals(list.remove("String 2"), "String 2");
        assertEquals(list.getSize(), 2);
    }

    @Test
    void addRemoveTest() {
        MyArrayList<String> list = new MyArrayList<>();

        for (int i = 0; i < 20; i++) {
            list.add("String " + i);
        }

        assertEquals(list.getSize(), 20);
        assertEquals(list.getCapacity(), 20);

        list.add("String 21");

        assertEquals(list.getSize(), 21);
        assertEquals(list.getCapacity(), 40);

        list.remove("String 12");
        list.remove("String 19");
        list.remove("String 21");

        assertEquals(list.getSize(), 18);
        assertEquals(list.getCapacity(), 40);

        list.displayContent();
    }

    @Test
    void getTest() {
        MyArrayList<String> list = new MyArrayList<>();

        assertThrows(NoSuchObjectException.class, () -> {
            list.get(-10);
        });

        assertThrows(NoSuchObjectException.class, () -> {
            list.get(10);
        });

        list.add("String 1");
        list.remove("String 1");

        assertEquals(list.getSize(), 0);
    }

    @Test
    void test() {
        MyArrayList<Integer> list = new MyArrayList<>();

        list.add(null);

        for (int i = 1; i <= 7; i++) {
            list.add(i);
        }

        list.displayContent();
        list.remove(9);
        list.displayContent();
    }







}