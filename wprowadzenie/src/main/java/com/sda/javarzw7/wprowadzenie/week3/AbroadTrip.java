package com.sda.javarzw7.wprowadzenie.week3;

import java.util.Date;

public class AbroadTrip extends Trip {

    private double insurance;

    public AbroadTrip(Date start, Date end, String destination, double price, double insurance) {
        super(start, end, destination, price);

        this.insurance = insurance;
        this.price = price + insurance;
    }

    public double getInsurance() {
        return insurance;
    }

    @Override
    public String toString() {
        return super.toString() + ", extra insurance included: " + insurance + " PLN";
    }
}
