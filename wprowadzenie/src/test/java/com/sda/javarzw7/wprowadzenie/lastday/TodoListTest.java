package com.sda.javarzw7.wprowadzenie.lastday;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TodoListTest {

    TodoList todoList;

    @BeforeEach
    void beforeEach() {
        todoList = new TodoList();

        todoList.add("Nauka Javy", "Wiecej czasu spedzic na nauce", Priority.HIGH);
        todoList.add("Nauka testowania kodu", "Przegladnac przyklady z zajec i wyprobowac na swoich klasach", Priority.MEDIUM);
        todoList.add("Odpoczynek", "Pojechac na jakies dobre wakacje", Priority.LOW);
    }

    @Test
    void add() {
        todoList.show();

        assertEquals(todoList.getSize(), 3);
        assertEquals(todoList.getTodo(2).getPriority(), Priority.MEDIUM);
    }

    @Test
    void remove() {
        todoList.remove(15);
        assertEquals(todoList.getSize(), 3);

        todoList.remove(2);
        assertEquals(todoList.getSize(), 2);

        todoList.add("Nauka testowania kodu", "Przegladnac przyklady z zajec i wyprobowac na swoich klasach", Priority.MEDIUM);
        todoList.show();

        todoList.remove(2);
        assertEquals(todoList.getSize(), 3);
    }

    @Test
    void markAsFinished() {
        todoList.markAsFinished(1);
        assertEquals(todoList.getTodo(1).isFinished(), true);
        todoList.show();
    }
}