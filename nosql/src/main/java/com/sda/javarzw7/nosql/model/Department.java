package com.sda.javarzw7.nosql.model;

public enum Department {
    IT,
    HR,
    PAYROLL;
}
