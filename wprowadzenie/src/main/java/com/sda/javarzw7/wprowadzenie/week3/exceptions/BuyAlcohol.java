package com.sda.javarzw7.wprowadzenie.week3.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class BuyAlcohol {

    private static final Scanner SCANNER = new Scanner(System.in);

    // aplikacja powinna pytac o wiek
    // uzytkownik powinien wpisac wiek
    // jesli wiek jest < 18, wyrzuc AlcoholNotAllowedException

    public static void main(String[] args) {

        int age = -1;

        try {
            age = askForAge();
            System.out.println("Alkohol sprzedany");
        } catch (AlcoholNotAllowedException e) {
            e.printStackTrace();
            System.out.println("Przepraszam, ale sprzedaz alkololu jest niemozliwa!");
        }

    }

    private static int askForAge() throws AlcoholNotAllowedException {
        System.out.println("Podaj swoj wiek:");

        int age = -1;

        try {
            age = SCANNER.nextInt();
        } catch (InputMismatchException e) {
            String userInput = SCANNER.nextLine(); // musimy zakonczyc czytanie linii do konca
            System.out.println("Metoda oczekiwala liczby calkowitej, a wpisales: '" + userInput + "', podaj ponownie ...");

            return askForAge();
        }

        if (age < 18) {
            throw new AlcoholNotAllowedException("Alkohol jest zabroniony dla osob nieletnich! Dzwonie po policje!");
        }

        return age;
    }
}

class AlcoholNotAllowedException extends Exception {
    public AlcoholNotAllowedException(String msg) {
        super(msg);
    }
}