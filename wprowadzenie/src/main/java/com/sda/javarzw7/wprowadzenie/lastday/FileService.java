package com.sda.javarzw7.wprowadzenie.lastday;

import java.io.*;

public class FileService implements Serializable {

    public static void saveTodoList(String fileName, TodoList todoList) {

        try (
                FileOutputStream fileOutputStream = new FileOutputStream(fileName);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        ) {

            objectOutputStream.writeObject(todoList);

        } catch (IOException e) {
            // TODO
            e.printStackTrace();
        }

    }

    public static TodoList readTodoList(String fileName) {

        TodoList todoList = null;

        try (
                FileInputStream fileInputStream = new FileInputStream(fileName);
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        ) {

            todoList = (TodoList) objectInputStream.readObject();

        } catch (IOException e) {
            // TODO
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO
            e.printStackTrace();
        }

        return todoList;
    }

}
