package com.sda.javarzw7.wprowadzenie.week3;

import java.util.Date;

public class DomesticTrip extends Trip {

    private int discount;

    public DomesticTrip(Date start, Date end, String destination, double price, int discount) {
        super(start, end, destination, price);

        this.discount = discount;
        this.price -= price * discount / 100;
    }

    public double getDiscount() {
        return discount;
    }

    @Override
    public String toString() {
        return super.toString() + ", extra discount included: " + discount + " PLN";
    }
}
