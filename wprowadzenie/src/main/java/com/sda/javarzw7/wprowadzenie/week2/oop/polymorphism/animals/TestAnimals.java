package com.sda.javarzw7.wprowadzenie.week2.oop.polymorphism.animals;

public class TestAnimals {

    public static void main(String[] args) {

        Animal[] animals = new Animal[4];

        Animal animal = new Animal("unknown");
        Animal dog = new Dog("Łatek");
        Animal cat = new Cat("Mruczek");
        Animal bird = new Bird("Tweety");

        animals[0] = animal;
        animals[1] = dog;
        animals[2] = cat;
        animals[3] = bird;

        for (Animal zwierzatko: animals) {
            zwierzatko.sound();
            System.out.println();
        }
    }
}
