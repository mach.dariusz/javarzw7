package com.sda.javarzw7.programowanie1.day3.lists;

import java.rmi.NoSuchObjectException;
import java.util.Arrays;

public class MyArrayList<T> {

    private final int DEFAULT_CAPACITY = 10;
    private int size;
    private int head;
    private T[] array;

    public MyArrayList() {
        this.array = (T[]) new Object[DEFAULT_CAPACITY];
    }

    public MyArrayList(int size) {
        this.array = (T[]) new Object[size];
    }

    public void add(T object) {
        if (size < array.length) {
            array[head] = object;
        } else {
            array = Arrays.copyOf(array, array.length * 2);
            array[head] = object;
        }

        head++;
        size++;
    }

    public T remove(T object) {
        if (!isEmpty()) {
            for (int i = 0; i < size; i++) {
                if (array[i] != null && array[i].equals(object)) {
                    T tempObject = array[i];

                    System.arraycopy(
                            array, i + 1, // wybieram tablice z ktorej kopiujemy obiekty, i index poczatkowy
                            array, i, // tablica i jej index gdzie docelowo trafiaja obiekty
                            array.length - i - 1 // ilosc elementow do skopiowania
                    );

                    size--;
                    head--;
                    return tempObject;
                }
            }
        }

        return null;
    }

    public T get(int index) throws NoSuchObjectException {
        if (index >= 0 && index < array.length) {
            return array[index];
        }

        throw new NoSuchObjectException("Nie ma takiego obiektu o podanym indeksie: " + index);
    }

    public void clear() {
        array = (T[]) new Object[DEFAULT_CAPACITY];

        size = 0;
        head = 0;
    }

    public void displayContent() {
        for (T item: array) {
            System.out.println(item);
        }
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int getSize() {
        return size;
    }

    public int getCapacity() {
        return array.length;
    }

}
