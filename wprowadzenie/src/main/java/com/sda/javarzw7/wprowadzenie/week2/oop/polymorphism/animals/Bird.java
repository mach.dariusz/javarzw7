package com.sda.javarzw7.wprowadzenie.week2.oop.polymorphism.animals;

public class Bird extends Animal {

    public Bird(String name) {
        super(name);
    }

    @Override
    public void sound() {
        System.out.printf("My name is %s, fiu fiu fiu", this.getName());
    }
}
