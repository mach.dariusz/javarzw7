package com.sda.javarzw7.wprowadzenie.week2.oop.interfaces;

public class MyCustomClass implements MyCustomInterface {
    @Override
    public void printValue(Object object) {
        System.out.println(object);
    }

    @Override
    public double multiply(int x, int y) {
        return x * y;
    }

    @Override
    public double multiply(double x, double y) {
        return x * y;
    }

    @Override
    public double divide(int x, int y) {
        if (x == 0 || y == 0) {
            throw new NumberFormatException("Wrong number, dont divide by 0!");
        }

        return x / y;
    }

    @Override
    public double divide(double x, double y) {
        if (x == 0 || y == 0) {
            throw new NumberFormatException("Wrong number, dont divide by 0!");
        }

        return x / y;
    }

    @Override
    public double sum(double x, double y) {
        return x + y;
    }
}
