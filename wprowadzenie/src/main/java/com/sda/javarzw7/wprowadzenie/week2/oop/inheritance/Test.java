package com.sda.javarzw7.wprowadzenie.week2.oop.inheritance;

public class Test extends Object {

    public static void main(String[] args) {
        Animal animal = new Animal("zwierze nieznanego gatunku");
        Dog dog = new Dog("Latek", "amstaf", 25);

        System.out.println(animal);
        System.out.println(dog);

    }
}
