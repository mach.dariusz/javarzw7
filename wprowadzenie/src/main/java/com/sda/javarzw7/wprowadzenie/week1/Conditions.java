package com.sda.javarzw7.wprowadzenie.week1;

public class Conditions {
    
    public static void main(String[] args) {
        wyjasnienie(null);
        wyjasnienie("");

        przyklad_1(false);
        przyklad_1(true);
       
        przyklad_2("kurs");
        przyklad_2("praca");
        przyklad_2("cos innego");
        przyklad_2(null);


        przyklad_3(1);
        przyklad_3(2);
        przyklad_3(3);
        przyklad_3(0);

    }

    private static void wyjasnienie(String argumentMetody) {
        String a = new String("A");
        String b = "A";
        System.out.println(a == b); // bedzie false
        System.out.println(a.equals(b)); // bedzie true
        String c = "A";
        System.out.println(b == c); // bedzie true
        System.out.println(b.equals(c)); // bedzie true -> zalecany sposob
        System.out.println(a.equals("a".toUpperCase())); // bedzie true
        
        // obsluga null i pustych Stringow
        if (argumentMetody != null && !argumentMetody.equals("")) {
            System.out.println("Przekazany argument jest null lub pusty String");
        }        
    }
    // if condition
    private static void przyklad_1(boolean argumentMetody) {
        // check if argumentMetody is equals to false
        if (!argumentMetody) {
            // if false then ...
            System.out.println("Przekazany argument jest rowny " + argumentMetody);
        } else {
            // if true then ...
            System.out.println("\n\tPrzekazany argument jest rowny " + argumentMetody);
        }
    }

    // TODO
    // metoda przyklad_2(String argumentMetody)
    // if "kurs" -> jestem na kursie, ucze sie javy
    // if "praca" -> teraz pracuje, jestem zajety 
    // nie mam pojecia co sie dzieje, chyba jestem przemeczony
    private static void przyklad_2(String argumentMetody) {
        if (argumentMetody != null) {
            
            if (argumentMetody.equals("kurs")) {
                System.out.println("jestem na kursie, ucze sie javy");
            } else if(argumentMetody.equals("praca")) {
                System.out.println("teraz pracuje, jestem zajety");
            } else {
                System.out.println("nie mam pojecia co sie dzieje, chyba jestem przemeczony");
            }
            
        }
    }

    private static void przyklad_3(int wartosc) {

        System.out.println();
        switch (wartosc) {
            case 1: 
                System.out.println("wykryto 1");
                break;
            case 2: 
                System.out.println("wykryto 2");
                break;
            case 3: 
                System.out.println("wykryto 3");
                break;
            default:
                System.out.println("wykryto cos innego");
        }

        System.out.println();
        switch (wartosc) {
            case 1: 
                System.out.println("wykryto 1");
            case 2: 
                System.out.println("wykryto 2");
            case 3: 
                System.out.println("wykryto 3");
            default:
                System.out.println("wykryto cos innego");
        }
    }


}