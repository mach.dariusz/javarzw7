package com.sda.javarzw7.programowanie1.day3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NWDTest {

    @Test
    void calc() {
        assertEquals(NWD.calc(2, 4), 2);
        assertEquals(NWD.calc(15, 100), 5);
    }
}