package com.sda.javarzw7.restapi.todos.model;

public class Todo {

    private final long id;
    private final String content;
    private boolean finished;

    public Todo(long id, String content) {
        this.id = id;
        this.content = content;
        this.finished = false;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
}
