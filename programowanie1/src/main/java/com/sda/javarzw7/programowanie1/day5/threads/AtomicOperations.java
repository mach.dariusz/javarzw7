package com.sda.javarzw7.programowanie1.day5.threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class AtomicOperations {

    static int counter = 0;

    public static void main(String[] args) {

        AtomicOperations ao = new AtomicOperations();
        ao.increment();
    }

    public void increment() {

        // AtomicInteger counter = new AtomicInteger(0);

        ExecutorService executorService = Executors.newFixedThreadPool(2);

        long start = System.nanoTime();
        IntStream.range(0, 10_000_000).forEach(i -> executorService.submit(() -> incrementCounter()));

        stop(executorService);
        long end = System.nanoTime();

        System.out.println(counter);
        System.out.println((end - start) / 1_000_000 );
    }

    public static void stop(ExecutorService executor) {
        try {
            executor.shutdown();
            executor.awaitTermination(60, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            System.err.println("termination interrupted");
        } finally {
            if (!executor.isTerminated()) {
                System.err.println("killing non-finished tasks");
            }
            executor.shutdownNow();
        }
    }

    static void incrementCounter() {
        counter++;
    }

}
