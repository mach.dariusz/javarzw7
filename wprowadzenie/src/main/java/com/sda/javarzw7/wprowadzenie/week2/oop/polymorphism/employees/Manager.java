package com.sda.javarzw7.wprowadzenie.week2.oop.polymorphism.employees;

public class Manager extends Employee {

    public Manager(String firstName, String lastName) {
        super(firstName, lastName, Type.MANGER);
    }
}
