package com.sda.javarzw7.wprowadzenie.week3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DateUtils {

    public static Date getDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date getDate(String localDateAsString) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd").parse(localDateAsString);
    }
}
