package com.sda.javarzw7.wprowadzenie.week3.staticblock;

public class App {

    static class A {}
    class B {}
}

class C {
    public static void main(String[] args) {
        App.A a = new App.A();
        App app = new App();

        App.B b = app.new B();
    }
}