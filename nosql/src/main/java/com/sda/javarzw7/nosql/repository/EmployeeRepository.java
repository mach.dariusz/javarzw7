package com.sda.javarzw7.nosql.repository;

import com.sda.javarzw7.nosql.model.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EmployeeRepository extends MongoRepository<Employee, String> {

    Employee findByLastNameAndFirstName(String lastName, String firstName);
}
