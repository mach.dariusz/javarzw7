package com.sda.javarzw7.nosql.service;

import com.sda.javarzw7.nosql.model.Employee;
import com.sda.javarzw7.nosql.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    /**
     * @param id
     * @return employee or null if not found
     */
    public Employee getEmployeeById(String id) {
        return employeeRepository.findById(id).orElse(null);
    }

    public Employee removeEmployeeById(String id) {
        Employee employee = employeeRepository.findById(id).orElse(null);
        employeeRepository.deleteById(id);

        return employee;
    }

    public Employee create(Employee employee) {
        return employeeRepository.save(employee);
    }
}
