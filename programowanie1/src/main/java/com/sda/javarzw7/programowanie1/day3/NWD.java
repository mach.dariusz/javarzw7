package com.sda.javarzw7.programowanie1.day3;

public class NWD {

    /**
     *
     * @param a
     * @param b
     * @return NWD for a and b
     */
    public static int calc(int a, int b) {

        while (a != b) {
            if (a < b) {
                b = b - a;
            } else {
                a = a - b;
            }
        }

        return a;
    }
}
