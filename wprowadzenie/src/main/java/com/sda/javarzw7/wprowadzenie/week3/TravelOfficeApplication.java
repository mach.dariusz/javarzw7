package com.sda.javarzw7.wprowadzenie.week3;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TravelOfficeApplication {

    private static final Scanner SCANNER = new Scanner(System.in);
    private static String username;

    public static void main(String[] args) {
        username = getUserName();
        System.out.println("Witaj w naszym biurze rezerwacji wycieczek " + username);

        showMenu();

        int userChoice = getUserChoice();

        switch (userChoice) {
            case 1:
                System.out.println("Podaj dane klienta");
                break;

            default:
                System.out.println("Nie mam takiej opcji!");
        }

    }

    private static void showMenu() {
        System.out.println("1. Dodaj nowego klienta");
        System.out.println("2. Dodaj nowa wycieczke");
        System.out.println("3. Dodaj nowa rezerwacje");
    }

    private static String getUserName() {
        System.out.println("\nPodaj nazwe uzytkownika:");

        return SCANNER.nextLine();
    }

    private static int getUserChoice() {
        System.out.println("\nWybierz opcje:");

        int result = -1;

        try {

            result = SCANNER.nextInt();

        } catch (InputMismatchException e) {
            e.printStackTrace();

            try {

                throw new WrongUserInputException("Metoda oczekiwala cyfry 1-7, cos poszlo nie tak ... Sprawdz co wpisales ...");

            } catch (WrongUserInputException ex) {
                ex.printStackTrace();

                String wrongUserInput = SCANNER.nextLine(); // dokoncz czytanie linii
                System.out.println("Wpisales blednie: " + wrongUserInput);

                System.out.println("Wybierz opcje z zakresu 1-9");
                return getUserChoice();
            }


        } catch (Exception exx) {
            exx.printStackTrace();
        }

        return result;
    }

}

class WrongUserInputException extends RuntimeException {

    public WrongUserInputException(String msg) {
        super(msg);
    }

}