package com.sda.javarzw7.wprowadzenie.week2.oop.inheritance;

public class Animal {
    private String name;
    private int age;

    public Animal(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Animal name is " + name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return this.age;
    }

    public String getName() {
        return this.name;
    }

}
