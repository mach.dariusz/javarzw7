package com.sda.javarzw7.nosql.controller;

import com.sda.javarzw7.nosql.model.Employee;
import com.sda.javarzw7.nosql.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employees")
    public List<Employee> getAll() {
        return employeeService.getAll();
    }

    @GetMapping("/employee/{id}")
    public Employee get(@PathVariable("id") String id) {
        return employeeService.getEmployeeById(id);
    }

    @DeleteMapping("/employee/{id}")
    public Employee remove(@PathVariable("id") String id) {
        return employeeService.removeEmployeeById(id);
    }

    @PostMapping("/employee")
    public Employee create(@RequestBody Employee employee) {
        return employeeService.create(employee);
    }


}
