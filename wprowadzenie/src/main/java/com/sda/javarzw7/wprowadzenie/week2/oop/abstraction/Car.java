package com.sda.javarzw7.wprowadzenie.week2.oop.abstraction;

public class Car extends Vehicle {

    public Car(String companyName) {
        super(companyName);
    }

    @Override
    public double getDistance() {
        return 700.00;
    }

    @Override
    public double getFuelUsage() {
        return 10.00;
    }

}
