package com.sda.javarzw7.wprowadzenie.week2.reservation;

public class TravelOffice {
    public static void main(String[] args) {
        Customer customer = new Customer("Jan Kowalski");
        customer.setAddress("Korczowa");

        Trip trip = new Trip(
                new Date(2019, 7, 22),
                new Date(2019, 7, 29),
                "Italy"
        );

        customer.assignTrip(trip);

        System.out.println(customer.getInfo());
    }
}
