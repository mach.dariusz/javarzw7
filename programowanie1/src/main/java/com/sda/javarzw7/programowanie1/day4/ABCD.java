package com.sda.javarzw7.programowanie1.day4;

import java.util.*;
import java.util.stream.Stream;

public class ABCD {

    public static List<Occurrence> search(char[][] array, String searchingText) {

        List<Occurrence> list = new ArrayList<>();

        checkAllOccurrencesOfSingleLetter(array, searchingText.charAt(0)).stream()
                .forEach(firstLetterOccurrence -> {

                    Stream.of(Direction.values()).forEach(direction -> {
                        Occurrence occurrence = searchWord(searchingText, 0, direction, array, firstLetterOccurrence, firstLetterOccurrence);

                        if (occurrence != null) {
                            System.out.printf("\nFound %s at %s in direction %s", searchingText, firstLetterOccurrence, direction);
                            list.add(occurrence);
                        }
                    });

                });

        return list;
    }

    public static Occurrence searchWord(String searchingWord, int letterPosition, Direction direction, char[][] array, Point startingPoint, Point nextPoint) {

        // direction EAST
        if (checkCharAt(array, searchingWord.charAt(letterPosition), nextPoint)) {
            // letter found, search next if needed
            if (searchingWord.length() - 1 == letterPosition) {
                // found searchingWord, return Occurrence
                return new Occurrence(startingPoint, nextPoint, direction);
            } else {
                if (checkIfContinueSearching(array, setNewPointBasedOnDirection(nextPoint, direction))) {
                    // try to search next letter of searchingWord
                    return searchWord(searchingWord, letterPosition + 1, direction, array, startingPoint, setNewPointBasedOnDirection(nextPoint, direction));
                }
            }
        }

        return null;
    }

    public static boolean checkIfContinueSearching(char[][] array, Point nextPoint) {
        int x = nextPoint.getX();
        int y = nextPoint.getY();

        boolean xCondition = x >= 0 && x < array.length;
        boolean yCondition = false;

        if (xCondition) {
            yCondition = y >= 0 && y < array[x].length;
        }

        if (xCondition && yCondition) {
            return true;
        } else {
            return false;
        }

    }

    public static Point setNewPointBasedOnDirection(Point point, Direction direction) {

        switch (direction) {
            case EAST:
                return new Point(point.getX(), point.getY() + 1);

            case WEST:
                return new Point(point.getX(), point.getY() - 1);

            case NORTH:
                return new Point(point.getX() - 1, point.getY());

            case SOUTH:
                return new Point(point.getX() + 1, point.getY());

            case NORTHEAST:
                return new Point(point.getX() - 1, point.getY() + 1);

            case NORTHWEST:
                return new Point(point.getX() - 1, point.getY() - 1);

            case SOUTHEAST:
                return new Point(point.getX() + 1, point.getY() + 1);

            case SOUTHWEST:
                return new Point(point.getX() + 1, point.getY() - 1);

            default:
                throw new ExpectedDirectionAsMethodArgumentException();
        }
    }

    public static boolean checkCharAt(char[][] array, char letter, Point point) {

        return letter == array[point.getX()][point.getY()];
    }

    public static List<Point> checkAllOccurrencesOfSingleLetter(char[][] array, char letter) {

        List<Point> list = new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {

                Point point = new Point(i, j);
                if (checkCharAt(array, letter, point)) {
                    // add to list of points
                    list.add(point);
                }

            }
        }

        return list;
    }


    public static char[][] generateRandomArray(int x, int y, String word) {

        Random random = new Random();

        char[][] array = new char[x][y];

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = word.charAt(random.nextInt(word.length()));
            }
        }

        return array;
    }

    public static void printArray(char[][] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println(Arrays.toString(array[i]));
        }
    }

}


class Occurrence {

    private Point startPosition;
    private Point endPosition;
    private Direction direction;

    public Occurrence(Point startPosition, Point endPosition, Direction direction) {
        this.startPosition = startPosition;
        this.endPosition = endPosition;
        this.direction = direction;
    }

    public Point getStartPosition() {
        return startPosition;
    }

    public Point getEndPosition() {
        return endPosition;
    }

    public Direction getDirection() {
        return direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Occurrence that = (Occurrence) o;
        return Objects.equals(startPosition, that.startPosition) &&
                Objects.equals(endPosition, that.endPosition) &&
                direction == that.direction;
    }

    @Override
    public int hashCode() {
        return Objects.hash(startPosition, endPosition, direction);
    }

    @Override
    public String toString() {
        return "Occurrence{" +
                "startPosition=" + startPosition +
                ", endPosition=" + endPosition +
                ", direction=" + direction +
                '}';
    }
}

enum Direction {
    NORTH,
    SOUTH,
    EAST,
    WEST,
    NORTHEAST,
    NORTHWEST,
    SOUTHEAST,
    SOUTHWEST
}

class Point {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x &&
                y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Point[" +
                "x=" + x +
                ", y=" + y +
                ']';
    }
}

class ExpectedDirectionAsMethodArgumentException extends RuntimeException {

}