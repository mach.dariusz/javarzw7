package com.sda.javarzw7.wprowadzenie.week2.oop.polymorphism.employees;

public class Employee {

    private String firstName;
    private String lastName;
    private double salary;
    private Type type;

    public Employee(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.type = Type.OTHER;
        this.salary = this.type.getSalary();
    }

    protected Employee(String firstName, String lastName, Type type) {
        this(firstName, lastName);
        this.type = type;
        this.salary = this.type.getSalary();
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public double getSalary() {
        return salary;
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Employee details:");
        stringBuilder.append("\n\tname: ").append(this.lastName).append(" ").append(this.firstName);
        stringBuilder.append("\n\ttype: ").append(this.type);
        stringBuilder.append("\n\tsalary: ").append(this.salary);

        return stringBuilder.toString();
    }
}
