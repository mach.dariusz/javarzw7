package com.sda.javarzw7.wprowadzenie.week2.oop.abstraction;

public class Plane extends Vehicle {

    public Plane(String companyName) {
        super(companyName);
    }

    @Override
    public double getDistance() {
        return 3500.00;
    }

    @Override
    public double getFuelUsage() {
        return 500.00;
    }

}
