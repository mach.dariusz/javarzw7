package com.sda.javarzw7.wprowadzenie.week2.oop.polymorphism;

public class Test {

    public static void main(String... args) {

        Employee testEmp = new Employee("Test", "Employee");
        System.out.println(testEmp);

        Employee employee = generateEmployee();
        Administrator administrator = generateAdministrator(employee);
        System.out.println(employee);
        System.out.println(administrator);

        // magia zaczyna sie tutaj
        magicHere(employee, administrator);

    }

    private static void magicHere(Employee employee, Administrator administrator) {
        System.out.println("**********");
        Employee employee2 = new Administrator("Jan", "Kowalski", Department.HR);

        ((Administrator) employee2).setAccess(Access.HIGH);
        System.out.println(employee2);


        Employee[] empsArray = new Employee[3];
        empsArray[0] = employee2; // Administrator na bazie Employee
        empsArray[1] = employee; // Employee
        empsArray[2] = administrator; // Administrator

        System.out.println();
        for (Employee emp: empsArray) {
            System.out.println(emp);
        }

        Administrator[] adminArray = new Administrator[2];
        adminArray[0] = administrator;
        adminArray[1] = (Administrator) employee2;
        // adminArray[2] = (Administrator) employee; // <- class cast exception here

        System.out.println("-----------");
        for (Administrator adm: adminArray) {
            System.out.println(adm);
        }
    }

    private static Administrator generateAdministrator(Employee employee) {
        Administrator administrator = new Administrator(
                employee.getFirstName(),
                employee.getLastName(),
                employee.getDepartment(),
                Access.LOW
        );
        return administrator;
    }

    private static Employee generateEmployee() {
        Employee employee = new Employee("Oskar", "Zaremba", Department.IT);
        return employee;
    }
}
