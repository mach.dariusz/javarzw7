package com.sda.javarzw7.wprowadzenie.week2.oop.interfaces.task;

public class Dog extends Animal {

    public Dog(String name) {
        super(name);
    }

    @Override
    public void feed() {
        System.out.println("dziekuje Ci moj Panie za tak pyszna kosteczke ...");
    }

    @Override
    public void sound() {
        System.out.println("hau hau hau");
    }
}
