package com.sda.javarzw7.programowanie1.day4;

public class MergeSort {

    public static void sort(int[] array, int arraySize) {
        if (arraySize < 2) {
            return;
        }

        // create mid and left, right arrays
        int mid = arraySize / 2;
        int[] left = new int[mid];
        int[] right = new int[arraySize - mid];

        // fill out arrays
        for (int i = 0; i < mid; i++) {
            left[i] = array[i];
        }

        for (int i = mid; i < arraySize; i++) {
            right[i - mid] = array[i];
        }

        sort(left, mid); // [5,1,6]
        sort(right, arraySize - mid); // [2,3,4] <- czeka

        merge(array, left, right, mid, arraySize - mid);
    }

    public static void merge(int[] array, int[] left, int[] right, int leftSize, int rightSize) {

        int i = 0; // left array position index
        int j = 0; // right array position index
        int k = 0; // main array position index

        // iterate over left and right array
        while (i < leftSize && j < rightSize) {
            if (left[i] <= right[j]) {
                // add to main array from left array
                array[k] = left[i];
                k++;
                i++;
            } else {
                // add to main array from right array
                array[k] = right[j];
                k++;
                j++;
            }
        }

        // move all elements from left array to main array
        while (i < leftSize) {
            array[k] = left[i];
            k++;
            i++;
        }

        // move all elements from right array to main array
        while (j < rightSize) {
            array[k] = right[j];
            k++;
            j++;
        }


    }

}




