package com.sda.javarzw7.wprowadzenie.week2.oop.interfaces.task;

public class TestInterface {

    // stworz interfejs IAnimal
    // void sound()

    // stworz abstrakcyjna klase Animal, ktora implementuje IAnimal
    // private String name
    // public Animal(String name)
    // public String getName()
    // public String toString()

    // stworz klase Dog, dziedzicz po klasie Animal

    // stworz klase Cat, dziedzicz po klasie Animal

    // utworz tablice obiektow IAnimal
    // dodaj obiekt Dog i Cat
    // wydrukuj wszystkie obiekty

    public static void main(String[] args) {
        IAnimal[] animals = {
                new Dog("Łatek"),
                new Cat("Filemon")
        };

        for (IAnimal animal: animals) {
            System.out.println(animal);
            animal.sound();
            ((Animal) animal).feed();
            System.out.println();
        }
    }
}
