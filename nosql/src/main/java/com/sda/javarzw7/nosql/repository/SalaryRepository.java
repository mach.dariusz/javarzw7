package com.sda.javarzw7.nosql.repository;

import com.sda.javarzw7.nosql.model.Salary;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SalaryRepository extends MongoRepository<Salary, String> {

}
