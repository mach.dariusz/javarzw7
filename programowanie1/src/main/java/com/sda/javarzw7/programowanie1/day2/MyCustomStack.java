package com.sda.javarzw7.programowanie1.day2;

public class MyCustomStack<T> implements IMyCustomStack<T> {

    private T[] stack;
    private int size;
    private int head;

    public MyCustomStack(int capacity) {
        this.stack = (T[]) new Object[capacity];
        this.size = 0;
        this.head = 0;
    }

    @Override
    public void push(T object) {
        if (isFull()) {
            throw new StackIsFullException();
        } else {
            this.stack[this.head] = object;
            this.head++;
            this.size++;
        }
    }

    @Override
    public T pop() {
        if (isEmpty()) {
            return null;
        } else {
            T tempObject = this.stack[this.head - 1];
            this.stack[this.head - 1] = null;
            this.size--;
            this.head--;
            return tempObject;
        }
    }

    @Override
    public T peek() {
        if (isEmpty()) {
            return null;
        } else {
            return this.stack[this.head - 1];
        }
    }

    public boolean isFull() {
        return this.size == this.stack.length;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public int getSize() {
        return this.size;
    }

    public int getCapacity() {
        return this.stack.length;
    }

    public void printStack() {
        // Arrays.stream(this.stack).filter(element -> element != null).forEach(System.out::println);

        for (int i = 0; i < this.size; i++) {
            System.out.println(this.stack[i]);
        }
    }


}

class StackIsFullException extends RuntimeException {

}
